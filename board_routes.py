import json

from flask import request, render_template, redirect, url_for

from board_app import app
from board_app import logged_user
from board_app import login as do_login

import board_model as model
from board_db import db


@app.route("/")
def index():
    return render_template('index.html')

from routes.the_board import *
from routes.manage_boards import *
from routes.user import *
