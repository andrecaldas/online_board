from flask import Flask
from flask import session

from config import Config

app = Flask(__name__, static_url_path='/static')
app.config.from_object(Config)

from board_db import User, GuestUser

def logged_user():
    if 'user_id' not in session:
        if 'guest_name' not in session:
            session['guest_name'] = 'Convidado'
        return GuestUser(session['guest_name'])
    return User.query.filter(User.id == session['user_id']).one()


def login(user_name, password):
    user = User.load(user_name, password)
    session['user_id'] = user.id
    return user


def logout():
    try:
        session.pop('user_id')
    except KeyError:
        pass
