import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SEND_FILE_MAX_AGE_DEFAULT = 0
    SECRET_KEY = 'very secret key'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'stuff' ,'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
