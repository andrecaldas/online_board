from datetime import datetime

from flask import session
from board_db import User, Course, Board, Message, BoardPeer, BoardActivityLog
from board_db import db


def create_user(db_session, name, email, password, print_name):
    user = User(
        name=name,
        email=email,
        print_name=print_name,
        password=password,
    )
    db_session.add(user)


def login(name, password):
    user = User.query.filter(User.name == name).one()
    return user


def create_course(db_session, owner, name, print_name, description):
    course = Course(
        owner=owner,
        name=name,
        print_name=print_name,
        description=description,
    )
    db_session.add(course)
    return course


def create_board(db_session, owner, course_name, name, print_name, description):
    course = get_user_course(owner, course_name)
    board = Board(
        course=course,
        name=name,
        print_name=print_name,
        description=description,
    )
    db_session.add(board)
    return board


def get_user_course(user, course_name):
    return Course.query\
        .filter(Course.owner == user)\
        .filter(Course.name == course_name)\
        .one()


def get_course(user_name, course_name):
    return Course.query\
        .filter(User.name == user_name)\
        .filter(Course.owner_id == User.id)\
        .filter(Course.name == course_name)\
        .one()


def get_board(owner_name, course_name, board_name):
    return Board.query\
        .filter(User.name == owner_name)\
        .filter(Course.owner_id == User.id)\
        .filter(Course.name == course_name)\
        .filter(Board.course_id == Course.id)\
        .filter(Board.name == board_name)\
        .one()


def append_message(db_session, board, user, message):
    message = Message(user=user, board=board, message=message)
    db_session.add(message)
    return message


def append_board_log(db_session, board, log_text):
    log = BoardActivityLog(user=user, board=board, log_text=log_text)
    db_session.add(log)
    return log


def get_board_logs(board, since):
    if since <= 0:
        since = datetime.fromtimestamp(datetime.utcnow().timestamp() + since)
    return BoardActivityLog.query\
        .filter(BoardActivityLog.board_id == board.id)\
        .filter(BoardActivityLog.timestamp > since)


def get_authorized_peers(board):
    return BoardPeer.query\
      .filter(BoardPeer.board_id == board.id)\
      .filter(BoardPeer.is_authorized_to_wirte == True)\
      .all()


def get_board_peers(db_session, board, user):
    result = {}
    result['waving_hand'] = [
        {
            'name': p.peer.name,
            'print_name': p.peer.print_name,
            'waving_hand_since': p.waving_hand_since.timestamp(),
        } for p in board.peers if p.is_waving_hand()
    ]
    result['others'] = [
        {
            'name': p.peer.name,
            'print_name': p.peer.print_name,
        } for p in board.peers if not p.is_waving_hand()
    ]
    return result


def check_board_peer(db_session, board, peer):
    if peer.is_guest():
        # TODO: count guests.
        return
    now = datetime.utcnow()
    board_peer = BoardPeer.query\
        .filter(BoardPeer.peer == peer)\
        .filter(BoardPeer.board == board)\
        .first()
    if board_peer is None:
        board_peer = BoardPeer(board=board, peer=peer, last_seen=now)
        db_session.add(board_peer)
    else:
        board_peer.last_seen = now
    db_session.commit()


def wave_hand(db_session, board, user, do_wave=True):
    board_peer = BoardPeer.query\
        .filter(BoardPeer.board == board)\
        .filter(BoardPeer.peer == user)\
        .one()
    if do_wave:
        board_peer.waving_hand_since = datetime.utcnow()
    else:
        board_peer.waving_hand_since = None
    db_session.commit()
    return board_peer.waving_hand_since


def is_peer_authorized(board, peer):
    return BoardPeer.query\
        .filter(BoardPeer.board == board)\
        .filter(BoardPeer.peer == peer)\
        .one().is_authorized_to_wirte


def authorize_peer(db_session, board, peer_name, do_authorize=True):
    board_peer = BoardPeer.query\
        .filter(BoardPeer.board == board)\
        .filter(User.name == peer_name)\
        .filter(BoardPeer.peer_id == User.id)\
        .one()
    board_peer.is_authorized_to_wirte = do_authorize
    board_peer.waving_hand_since = None
    db_session.commit()
    return board_peer.is_authorized_to_wirte
