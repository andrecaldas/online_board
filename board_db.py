from random import randint

from werkzeug.security import generate_password_hash, check_password_hash

from sqlalchemy import UniqueConstraint
from sqlalchemy import and_

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy.ext.declarative import declared_attr

from board_app import app
from config import Config

db = SQLAlchemy(app)
migrate = Migrate(app, db)

class GuestUser():
    name = 'guest'
    email = 'unknown'

    def __init__(self, print_name):
        self.print_name = print_name

    @classmethod
    def is_guest(self):
        return True

class ABoardMixin(object):
    @declared_attr
    def __table_args__(cls):
        return ({'mysql_charset' : 'utf8mb4'},)


class User(ABoardMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True, nullable=False)
    email = db.Column(db.String(120), index=True, unique=True, nullable=False)
    print_name = db.Column(db.String(64), nullable=False)
    password_hash = db.Column(db.String(512), nullable=False)

    def __init__(self, name, email, print_name, password):
        super().__init__(
            name=name,
            email=email,
            print_name=print_name,
            password_hash=generate_password_hash(password),
        )

    @classmethod
    def is_guest(self):
        return False

    def get_course(self, id_string):
        return self.courses\
            .filter(Course.owner_id == self.id)\
            .filter(Course.id_string == id_string)\
            .one()

    @classmethod
    def load(cls, name, password):
        candidate = cls.query.filter(cls.name == name).one()
        if not check_password_hash(candidate.password_hash, password):
            raise Exception('Wrong password!')
        return candidate

    def __repr__(self):
        return '<User {}>'.format(self.name)


class Course(ABoardMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # Unique owner + name
    owner_id = db.Column(db.ForeignKey(User.id), nullable=False)
    owner = db.relationship(User, backref="courses")
    name = db.Column(db.String(16), nullable=False)
    print_name = db.Column(db.String(64), nullable=False)
    description = db.Column(db.String(1024), nullable=False)

    def get_board(self, id_string):
        return self.board\
            .filter(Board.course_id == self.id)\
            .filter(Board.id_string == id_string)\
            .one()

    def __repr__(self):
        return '<Course {}/{}>'.format(self.owner.name, self.name)
UniqueConstraint(Course.owner_id, Course.name)


class Board(ABoardMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # Unique course + name
    message_id_shift = db.Column(db.Integer, default=(lambda: randint(100000, 1000000)))
    course_id = db.Column(db.ForeignKey(Course.id), nullable=False)
    course = db.relationship(Course, backref="boards")
    name = db.Column(db.String(16), nullable=False)
    print_name = db.Column(db.String(64), nullable=False)
    description = db.Column(db.String(1024), nullable=False)

    def get_message(self, scrambled_id):
        return Message.query\
            .filter(Message.id == scrambled_id - self.message_id_shift)\
            .filter(Message.id == self.id)\
            .one()

    def get_messages_after_scrambled_id(self, scrambled_id):
        return Message.query\
            .filter(Message.board_id == self.id)\
            .filter(Message.id > (scrambled_id - self.message_id_shift))\
            .all()

    def append_message(self, db_session, user, message_text):
        message = Message(
            user=user,
            board=self,
            text=message_text,
        )
        db_session.add(message)

    def __repr__(self):
        return '<Board {}/{}/{}>'.format(
            self.course.owner.name,
            self.course.name,
            self.name,
        )
UniqueConstraint(Board.course_id, Board.name)


class Message(ABoardMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey(User.id), nullable=False)
    # TODO: this backref has to be properly setup.
    user = db.relationship(User, backref="messages")
    board_id = db.Column(db.ForeignKey(Board.id), nullable=False)
    board = db.relationship(Board, backref="messages")
    text = db.Column(db.String(1024), nullable=False)

    def scrambled_id(self):
        return self.id + self.board.message_id_shift

    def __repr__(self):
        return '<Message {}>'.format(self.id)


class BoardActivityLog(ABoardMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    board_id = db.Column(db.ForeignKey(Board.id), nullable=False)
    log_text = db.Column(db.String(256), nullable=False)
    # TODO: order by timestamp.
    #   Precompile query and setup index.
    timestamp = db.Column(db.TIMESTAMP, nullable=False)


class BoardPeer(ABoardMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # TODO: unique indexed key board_id + user_id.
    peer_id = db.Column(db.ForeignKey(User.id), nullable=False)
    peer = db.relationship(User)
    board_id = db.Column(db.ForeignKey(Board.id), nullable=False)
    board = db.relationship(Board, backref="peers")
    waving_hand_since = db.Column(db.TIMESTAMP, default=None)
    is_authorized_to_wirte = db.Column(db.Boolean, default=False, nullable=False)
    last_seen = db.Column(db.TIMESTAMP, nullable=False)

    def is_waving_hand(self):
        return (self.waving_hand_since is not None)

    def __repr__(self):
        return '<BdPr {}/{}>'.format(self.board_id, self.peer_id)
UniqueConstraint(BoardPeer.board_id, BoardPeer.peer_id)
