// What shall we do when focus is lost and then reaquired?
// Maybe we should push when we get focus and pop when we lose focus.
// Then, we shall not need those === undefined.
$.keyboard.push_keyaction = function (base, return_value, new_keyset, return_to_keyset) {
  if (new_keyset === undefined)
  {
    new_keyset = base.keyset_stack[base.keyset_stack.length-1]
  }

  if (return_to_keyset === undefined)
  {
    return_to_keyset = base.keyset_stack[base.keyset_stack.length-1]
  }

  base.keyset_stack.push(new_keyset)
  base.layout_stack.push({
    'keyset': return_to_keyset,
    'return_value': return_value,
  })

  base.$keyboard
    .find('.ui-keyboard-keyset').hide().end()
    .find('.ui-keyboard-keyset-' + new_keyset).show().end()
}

$.keyboard.pop_keyaction = function (base) {
  current_keyset = (base.keyset_stack.length != 0) ? base.keyset_stack.pop() : 'default'
  back_to = base.layout_stack.pop()

  let return_value = back_to.return_value
  if (typeof return_value === 'function')
  {
    // TODO: look for cursor position.
    return_value = return_value(base)
  }
  base.insertText(return_value)

  base.$keyboard
    .find('.ui-keyboard-keyset').hide().end()
    .find('.ui-keyboard-keyset-' + back_to.keyset).show().end()
}


$.extend( $.keyboard.keyaction, {
  layout_back: function (base) {
    $.keyboard.pop_keyaction(base)
  },

  start_math: function (base) {
    new_node = new TexFormula()
    base.current_node.insert_after(new_node)
    base.current_node = new_node
// TODO: Manage keyset change.
//    $.keyboard.push_keyaction(base, '$', 'basic_math')
  },

  basic_math: function (base) {
    $.keyboard.push_keyaction(base, '', 'basic_math')
  },

  parentheses: function (base) {
    base.current_node.
    base.insertText('\\left(')
    // TODO: feedback.
    $.keyboard.push_keyaction(base, '\\right)')
  },

  letters: function (base) {
    $.keyboard.push_keyaction(base, '', 'letters')
  },

  others: function (base) {
    $.keyboard.push_keyaction(base, '', 'others')
  },

  matrix: function (base) {
    base.insertText('\\begin{pmatrix}')
    $.keyboard.push_keyaction(base, '\\end{pmatrix}', 'matrix')
  },

  matrix_column: function (base) {
    base.insertText(' & ')
  },

  matrix_line: function (base) {
    base.insertText(' \\\\ ')
  },

  integral: function (base) {
    base.insertText('\\int ')
    // TODO: layout for "close_integral" instead of letters.
    $.keyboard.push_keyaction(base, '', 'letters')
    $.keyboard.push_keyaction(base, '\\mathrm{d}', 'integral')
  },

  close_integral: function (base) {
    $.keyboard.pop_keyaction(base)
  },

  integral_subscript: function (base) {
    // TODO: do not accept after != \int
    base.insertText('_{')
    // TODO: feedback.
    $.keyboard.push_keyaction(base, '}')
  },

  integral_superscript: function (base) {
    // TODO: do not accept after != \int or _
    base.insertText('^{')
    // TODO: feedback.
    $.keyboard.push_keyaction(base, '}')
  },

  frac: function (base) {
    base.insertText('\\frac{')
    // TODO: layout for "close_integral" instead of letters.
    $.keyboard.push_keyaction(base, '}')
    $.keyboard.push_keyaction(base, '}{')
  },

  sqrt: function (base) {
    base.insertText('\\sqrt{')
    // TODO: layout for "close_integral" instead of letters.
    $.keyboard.push_keyaction(base, '}')
  },

  log: function (base) {
    base.insertText('\\mathrm{log} ')
  },

  exp: function (base) {
    base.insertText('{')
    // TODO: layout for "close_integral" instead of letters.
    $.keyboard.push_keyaction(base, '}')
    $.keyboard.push_keyaction(base, '}^{')
  },

  times: function (base) {base.insertText('\\times ')},
  expbase: function (base) {base.insertText('\\mathrm{e}')},
  det: function (base) {base.insertText('\\mathrm{det}')},

  alpha: function (base) {base.insertText('\\alpha ')},
  beta: function (base) {base.insertText('\\beta ')},
  gamma: function (base) {base.insertText('\\gamma ')},
  epsilon: function (base) {base.insertText('\\varepsilon ')},
  eta: function (base) {base.insertText('\\eta ')},
  ell: function (base) {base.insertText('\\ell ')},
  pi: function (base) {base.insertText('\\pi ')},

  _x_: function (base) {base.insertText('(x)')},
  _y_: function (base) {base.insertText('(y)')},
  _z_: function (base) {base.insertText('(z)')},
  _a_: function (base) {base.insertText('(a)')},
  _ab_: function (base) {base.insertText('(a,b)')},
  _abc_: function (base) {base.insertText('(a,b,c)')},
  _xy_: function (base) {base.insertText('(x,y)')},
  _xyz_: function (base) {base.insertText('(x,y,z)')},


  close_curly: function (base) {base.insertText('}')},
  close_open_curly: function (base) {base.insertText('}{')},

  end: function (base) {
    let input = base.$preview.val()
    input = input.replace(/</g, '').replace(/>/g, '')
    input = input.replace(/\\begin\{/g, '<').replace(/\\end\{/, '>')
    let back_count = 0
    last_curly = input.length
    let tag_name_with_curly = ''
    for (let i = input.length-1; i >= 0; --i)
    {
      if (input[i] === '<')
      {
        if (0 === back_count)
        {
          tag_name_with_curly = input.substring(i+1, last_curly) + '}'
          break
        }
        --back_count
      }
      else if (input[i] === '>') ++back_count
      else if (/[}]/.test(input[i])) last_curly = i
    }

    base.insertText('\\end{' + tag_name_with_curly)
  },
})

alternative_keysets_keys = '{start_math} {basic_math} {letters} {ial} {anterior}'
alternative_keysets_keys += ' {matrix_column} {matrix_line}'
alternative_keysets_keys += ' {integral_subscript} {integral_superscript}'

$('#board_input').keyboard({
  layout: 'custom',
  autoAccept: true,
//  usePreview: false,
  appendLocally: true,
  keyBinding: $.keyboard.defaultOptions.keyBinding + ' change',
  change: function(e, keyboard, el) {preview_input(keyboard.preview)},
  enterNavigation: true, //< shift + enter

  customLayout: {
    'default': [
      alternative_keysets_keys,
      "1 2 3 4 5 6 7 8 9 0 \u0027 \u00ab",
      "a(@) s d f g h j k l \u00e7 \u00ba \u007e {enter}",
      "{parentheses} {accept}"
    ],
    'xxdefault': [
      alternative_keysets_keys,
      "1 2 3 4 5 6 7 8 9 0 \u0027 \u00ab",
      "{tab} q w e r t y u i o p \u00b4 {b}",
      "a s d f g h j k l \u00e7 \u00ba \u007e {enter}",
      "{shift} \\ z x c v b n m \u002c \u002e \u002d {shift}",
      "{start_math} {space} {alt} {combo} {accept}"
    ],
    'shift' : [
      alternative_keysets_keys,
      "\u007c \u0021 \u0022 \u0023 \u0024 \u0025 \u0026 \u002f \u0028 \u0029 \u003d \u003F \u00bb {bksp}",
      "{tab} Q W E R T Y U I O P \u002a \u0060",
      "A S D F G H J K L \u00c7 \u00aa \u005e {enter}",
      "{shift} \u003e Z X C V B N M \u003b \u003a \u005f {shift}",
      "{start_math} {space} {alt} {combo} {accept}"
    ],
    // AltGr
    'alt' : [
      alternative_keysets_keys,
      "\u005C 1 \u0040 \u00a3 \u00a7 \u20ac 6 \u007b \u005b \u005d \u007d \u0027 \u00ab {bksp}",
      "{tab} q w \u20ac r t y u i o p \u0308 \u00b4",
      "a s d f g h j k l \u00e7 \u00ba \u007e {enter}",
      "{shift} \u003c z x c v b n m \u002c \u002e \u002d {shift}",
      "{start_math} {space} {alt} {combo} {accept}"
    ],
    'alt-shift' : [
      alternative_keysets_keys,
      "\u007c \u0021 \u0022 \u0023 \u0024 \u0025 \u0026 \u002f \u0028 \u0029 \u003d \u003F \u00bb {bksp}",
      "{tab} Q W \u20ac R T Y U I O P \u0308 \u0060",
      "A S D F G H J K L \u00c7 \u00aa \u005e {enter}",
      "{shift} \u003e Z X C V B N M \u003b \u003a \u005f {shift}",
      "{start_math} {space} {alt} {combo} {accept}"
    ],

    'basic_math': [
      alternative_keysets_keys,
      '1 2 3 4 5 6 7 8 9 0 . ,',
      '+ - {frac} {times} {exp} {sqrt} {log} = \'',
      'x y z a b c f g h k {parentheses}',
      '{sin} {cos} {tg} {arcsin} {arccos} {arctg}',
      '{pi} {expbase} {ell} {alpha} {beta} {gamma} {epsilon} {delta} {eta}',
    ],
    
    'ial': [
      alternative_keysets_keys,
      '{cancel_latex_edition} {delete_latex_node} {accept_latex_edition}',
      'I A L 4 5 6 7 8 9 0 . ,',
      '+ - {frac} {times} {exp} {sqrt} {log} = \'',
      'x y z a b c f g h k {parentheses}',
      '{sin} {cos} {tg} {arcsin} {arccos} {arctg}',
      '{pi} {expbase} {ell} {alpha} {beta} {gamma} {epsilon} {delta} {eta}',
      '{layout_back}',
    ],
    
    'letters': [
      alternative_keysets_keys,
      'a b c {sp:.2} p q r s         t',
      'x y z {sp:.2} u v w {expbase} {pi}',
      'f g h {sp:.2} i j k m         n',
      '{alpha} {beta} {gamma} {sp:.2} {epsilon} {delta} {eta} {ell}',
      '{shift!!} {alt!!} {space} {layout_back}',
    ],

    'letters-shift': [
      alternative_keysets_keys,
      'A B C {sp:.2} P Q R S         T',
      'X Y Z {sp:.2} U V W E         {pi}',
      'F G H {sp:.2} I J K M         N',
      '{alpha} {beta} {gamma} {sp:.2} {epsilon} {delta} {eta} {ell}',
      '{shift!!} {alt!!} {space} {layout_back}',
    ],

    'letters-alt': [
      alternative_keysets_keys,
      'X X X {sp:.2} P Q R S         T',
      'X X X {sp:.2} U V W E         {pi}',
      'X X X {sp:.2} I J K M         N',
      '{alpha} {beta} {gamma} {sp:.2} {epsilon} {delta} {eta} {ell}',
      '{shift!!} {alt!!} {space} {layout_back}',
    ],

    'integral': [
      alternative_keysets_keys,
      '{integral_subscript} {integral_superscript} {frac} {sqrt}',
      'x y z {_x_} {_y_} {_z_} {_a_} {_ab_} {_abc_} {_xy_} {_xyz_}',
      'f g h {sp:.2} a b c {sp:.2} f      g      h',
      '{change_int} {change_iint} {change_iiint} {change_oint} {change_oiint} {change_oiiint}',
    ],

    'matrix': [
      alternative_keysets_keys,
      '1 2 3 {sp:.2} {frac}      {sp:.2} x     y        z',
      '4 5 6 {sp:.2} +           {sp:.2} a     b        c',
      '7 8 9 {sp:.2} -           {sp:.2} i     j        k',
      '0 . , {sp:.2} {subscript} {sp:.2} {exp}',
    ],
  },


  display: {
    'start_math': 'Fórmula:Escrever Fórmulas Matemáticas',

    'end': '\\end',
    'times': '$\\times$',
    'matrix': '$\\left[\\square\\right]$',
    'matrix_column': '$\\left[|\\right]$',
    'matrix_line': '$\\left[--\\right]$',

    'integral': '$\\int\\square$',
    'change_integral': '$\\int$',
    'change_iintegral': '$\\iint$',
    'change_iiintegral': '$\\iiint$',
    'integral_subscript': '$\\int_{\\square}$',
    'integral_superscript': '$\\int^{\\square}$',

    'frac': '$\\frac{\\square}{\\square}$',
    'sqrt': '$\\sqrt{\\scriptstyle\\square}$',
    'expbase': '$\\mathrm{e}$',
    'exp': '${\\square}^{\\square}$',
    'log': '$\\mathrm{log}$',
    'det': '$\\mathrm{det}$',

    'parentheses': '$\\lparen\\square\\rparen$',
    'close_curly': '}',
    'close_open_curly': '}{',
    'close_integral': '$\\rightarrow$:Pronto',

    'sin': '$\\mathrm{sen}$',
    'cos': '$\\mathrm{cos}$',
    'tg': '$\\mathrm{tg}$',

    'alpha': '$\\alpha$',
    'beta': '$\\beta$',
    'gamma': '$\\gamma$',
    'epsilon': '$\\varepsilon$',
    'delta': '$\\delta$',
    'eta': '$\\eta$',
    'ell': '$\\ell$',
    'pi': '$\\pi$',

    '_x_': '$\\lparen{x}\\rparen$',
    '_y_': '$\\lparen{y}\\rparen$',
    '_z_': '$\\lparen{z}\\rparen$',
    '_a_': '$\\lparen{a}\\rparen$',
    '_ab_': '$\\lparen{a,b}\\rparen$',
    '_abc_': '$\\lparen{a,b,c}\\rparen$',
    '_xy_': '$\\lparen{x,y}\\rparen$',
    '_xyz_': '$\\lparen{x,y,z}\\rparen$',

    'basic_math': 'Básico:Expressões usadas em todo lugar',
    'ial': 'Álgebra:Expressões usadas em álgebra linear',
    'letters': 'Letras:Letras comumente utilizadas em matemática',
    'others': 'Outros:Outros teclados matemáticos',

    'layout_back': 'Teclado Anterior',
  }
});

// append methods to keyboard
{
  let keyboard = $.data($('#board_input')[0]).keyboard

  // Hack to allways accept even if esc is pressed.
  old_close = keyboard.close
  keyboard.close = function(accept) {
    return old_close(true)
  }

  // Hack to trigger MathJax only after keyboard is constructed.
  old_startup = keyboard.startup
  keyboard.startup = function() {
    old_startup()
    MathJax.typesetPromise($('.ui-keyboard-text').toArray())

    console.log($.keyboard)
    var layouts = $.keyboard.builtLayouts
    for (l in layouts)
    {
      // Really very ugly hack!
      var keys = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'ç', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
      for (let i=0; i < keys.length; ++i)
      {
        layouts[l].mappedKeys[keys[i]] = keys[i]
      }
    }
  }

  // Hack to insert input data into tex_tree.
  old_insertText = keyboard.insertText
  keyboard.insertText = function(txt) {
    // For some reason, backspace is passed as text! :-(
    if ('bksp' === txt)
    {
      // TODO: treat backspace.
    }
    else
    {
      letter_node = new TexNodeLetter(txt)
      keyboard.current_node.insert_after(letter_node)
      keyboard.current_node = letter_node
    }
    old_insertText(txt)
  }

  // Stack to open/close LaTeX blocks.
  keyboard.tex_root = new TexNodeRoot()
  keyboard.current_node = keyboard.tex_root.first_child
  keyboard.tex_root.default_keyset = 'default'
  keyboard.layout_stack = ['default']
}
