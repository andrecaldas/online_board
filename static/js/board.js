// TODO: use Lodash.
// Atention: shall not be used inside html paremeters.
function escape_html (txt) {
  return txt.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;')
}

function post_line ()
{
  const request = new XMLHttpRequest();
  request.open('POST', 'append_text');
  const form_data = new FormData(document.querySelector('#board_form'));
  input_element = document.querySelector('#board_input');
  input_element.value = '';
  input_element.setAttribute('value', '');
  reset_tex_tree()
  request.send(form_data);
}


feedback_queue = []
next_feedback = 0
function print_next_feedback ()
{
  now = new Date().getTime()
  if (now < next_feedback)
  {
    return
  }
  next_feedback = now + 2000

  element = $('#feedback')[0]
  element.style.display = 'none'
  element.innerHTML = ''
  next = feedback_queue.shift()
  if (next)
  {
    element.innerText = next
  element.style.display = 'block'
  }
}


function ajax_login(form)
{
  var data = {}
  $(form).serializeArray().forEach(x => {data[x.name] = x.value})
  $.post({
    url : '/login',
    data: data,
    success : () => {update_user_info()},
  })
}


// TODO: move it from here!
preview_input_promise = Promise.resolve()
old_input = ''
preview_input_html = ''
function typeset_preview ()
{
  preview_element = $('#input_preview')[0]

  if(preview_input_html === old_input)
  {
    return
  }

  preview_input_promise = preview_input_promise.then(() => {
    if(preview_input_html === old_input)
    {
      return Promise.resolve()
    }
    old_input = preview_input_html

    preview_element.innerHTML = preview_input_html
    return MathJax.typesetPromise([preview_element])
  })
}

function preview_input (input)
{
  preview_input_html = input
}

// Cargo-culture: it seems MathJax does not load \class macro
// when keyboard is typesetting its math.
// When we push the button "fórmula", Vue builds the keyboard
// and at the same time, ${\class{formula_math}{}$ is previewed.
// I am to tired to figure out the proper way to make MathJax load \class.
var hack_element = document.createElement('div')
hack_element.innerHTML = '$\\class{x}{}$'
MathJax.typesetPromise([hack_element])
setTimeout(() => hack_element = undefined, 10000)


$(document).ready(
  () => {
    setInterval(print_next_feedback, 500);
    setInterval(typeset_preview, 500);
  }
)
