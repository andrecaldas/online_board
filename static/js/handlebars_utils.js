/*
* Thanks to:
* https://berzniz.com/post/24743062344/handling-handlebarsjs-like-a-pro
*/

Handlebars.changeElementInnerHTML = function(element, template_name, args, callback)
{
  if (Handlebars.templates === undefined) {
    Handlebars.templates = {};
  }

  if (Handlebars.templates[template_name] === undefined) {
    $.get({
cache: false,
      url : '/static/handlebars/' + template_name + '.handlebars',
      success : function(data) {
        Handlebars.templates[template_name] = Handlebars.compile(data);
        element.innerHTML = Handlebars.templates[template_name](args);
        if (callback !== undefined)
        {
          callback()
        }
      },
    });
  }
  else
  {
    element.innerHTML = Handlebars.templates[template_name](args);
    if (callback !== undefined)
    {
      callback()
    }
  }
};
