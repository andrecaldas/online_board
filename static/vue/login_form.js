"use strict";

Vue.component('login_form', {
  props: [],
  methods: {
    do_login () {
      var data = {}
      $.post({
        url : '/login',
        data: {user_name: this.user_name, password: this.password},
        success : () => {this.$emit('user_change')},
      })
    }
  },

  data () { return {
    user_name: '',
    password: '',
  }},

  template: `
    <form class="login_form" @submit.prevent="do_login" method="post" action="/login">
      <input type="text" v-model="user_name" placeholder="Login" />
      <input type="password" v-model="password" placeholder="Senha" />
      <input type="submit" value="Entrar" />
    </form>
`
})
