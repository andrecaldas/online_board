"use strict";

Vue.component('peer_panel', {
  props: {
    user_info: {type: Object, required: true},
  },

  methods: {
    wave_hand () {
      wave_hand()
    },

    lower_hand () {
      lower_hand()
    },
  },

  template: `
    <div v-if="user_info.can_send_message" id="input_box" class="user_panel">
      <form id="board_form" action="append_text" method="post"
          onsubmit="post_line(); preview_input(''); return false;">
        <tex_input
          placeholder="Digite texto com código latex!">
        </tex_input>
      </form>
      <div id="input_preview" tabindex="-1"></div>
    </div>
    <div v-else class="user_panel">
      <ul>
        <li v-if="!user_info.has_hands_up"><button @click="wave_hand">Levantar a Mão</button></li>
        <li v-if="user_info.has_hands_up"><button @click="lower_hand">Abaixar a Mão</button></li>
      </ul>
    </div>
  `
})
