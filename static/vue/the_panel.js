"use strict";

Vue.component('the_panel', {
  props: {
    owner_name: {type: String, required: true},
    user_info: {type: Object, required: true},
  },

  computed: {
    is_owner () {
      return (this.owner_name === this.user_info.user_name)
    },

    is_peer () {
      return (this.user_info.user_name !== '' && this.user_info.user_name !== this.owner_name)
    },

    is_guest () {
      return (this.user_info.user_name === '')
    },
  },

  template: `
    <div id="the_panel">
      <owner_panel v-if="is_owner"
        v-on="$listeners"
      ></owner_panel>
      <peer_panel v-else-if="is_peer"
        :user_info="user_info"
        v-on="$listeners"
      ></peer_panel>
      <guest_panel v-if="is_guest"
        v-on="$listeners"
      ></guest_panel>
    </div>
  `
})
