"use strict";

/*
* Waving hand peer.
*/
var component_waving_hand_peer = {
  props: {
    peer: {type: Object, required: true},
    is_owner: {type: Boolean, required: true},
  },

  methods: {
    authorize_peer: function() {
      // TODO: onload sinalisar que solicitação foi enviada.
      const request = new XMLHttpRequest();
      request.open('POST', 'authorize_peer');
      const form_data = new FormData();
      form_data.append('peer_name', this.peer.name);
//      request.responseType = 'json';
      request.send(form_data);
    },

    revoke_peer: function () {
      // TODO: onload sinalizar que solicitação foi enviada.
      const request = new XMLHttpRequest();
      request.open('POST', 'revoke_peer');
      const form_data = new FormData();
      form_data.append('peer_name', this.peer.name);
//      request.responseType = 'json';
      request.send(form_data);
    }
  },

  template: `
    <ul>
      <li>{{ peer.print_name }}</li>
      <li v-if="is_owner"><button @click="authorize_peer">Autorizar</button></li>
      <li v-if="is_owner"><button @click="revoke_peer">Revogar autorização</button></li>
    </ul>
  `
}


/*
* Waving hand peers.
*/
var component_waving_hand_peers = {
  props: {
    peers: { type: Array, default: [] },
    is_owner: { type: Boolean, required: true },
  },

  computed: {
    ordered_peers: function () {
      function compare_wavers (a, b)
      {
        if (a.waving_hand_since < b.waving_hand_since)
        {
          return -1
        }

        if (a.waving_hand_since > b.waving_hand_since)
        {
          return 1
        }

        return a.print_name.localeCompare(b.print_name)
      }

      return this.peers.sort(compare_wavers)
    },
  },

  components: {
    waving_hand_peer: component_waving_hand_peer,
  },

  template: `
    <ol v-if="peers.length">
<li>Waving peers</li>
      <li v-for="peer in ordered_peers">
        <waving_hand_peer
          :peer="peer"
          :is_owner="is_owner"
        ></waving_hand_peer>
      </li>
    </ol>
  `
}


/*
* Non-waving hand peer.
*/
var component_other_peer = {
  props: {
    peer: {
      type: Object,
      required: true,
    },
  },

  template: '<span>{{ peer.print_name }}</span>'
}


/*
* Non-waving hand peers.
*/
var component_other_peers = {
  props: {
    peers: {
      type: Array,
      default: [],
    },
  },

  computed: {
    ordered_peers: function () {
      function compare_non_wavers (a, b)
      {
        return a.print_name.localeCompare(b.print_name)
      }

      return this.peers.sort(compare_non_wavers)
    },
  },

  components: {
    other_peer: component_other_peer,
  },

  template: `
    <ol v-if="peers.length">
      <li v-for="peer in ordered_peers">
        <other_peer :peer="peer"></other_peer>
      </li>
    </ol>
  `
}


/*
* Main component: peer list.
*/
Vue.component('peer_list', {
  props: {
    user_info: {type: Object, required: true},
    waving_hand: {type: Array, required: true},
    others: {type: Array, required: true},
  },

  computed: {
    update_miliseconds () {
      return this.user_info.is_owner ? 5000 : 60000
    }
  },

  components: {
    waving_hand_peers: component_waving_hand_peers,
    other_peers: component_other_peers,
  },

  template: `
    <div>
      <ol v-if="waving_hand.length">
        <waving_hand_peers
          :peers="waving_hand"
          :is_owner="user_info.is_owner"
        ></waving_hand_peers>
      </ol>

      <ul v-if="others.length">
        <other_peers :peers="others"></other_peers>
      </ul>
    </div>
  `
})
