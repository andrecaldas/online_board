"use strict";

var board_app = new Vue({
  el: '#board_main',
  data () { return {
    messages: [],
    last_checked_out_message_id: -1,
    authorized_peers: [],
    owner_name: _OWNER_NAME.concat(),
    user_name: '',
    waving_hand_peers: [],
    other_peers: [],
  }},

  computed: {
    is_owner () {
      return this.user_name === this.owner_name
    },

    is_guest () {
      return this.user_name === ''
    },

    can_send_message () {
      return this.authorized_peers.includes(this.user_name)
    },

    has_hands_up () {
      return this.waving_hand_peers.some(info => info.name === this.user_name)
    },

    user_info () {
      return {
        user_name: this.user_name,
        can_send_message: this.can_send_message,
        is_guest: this.is_guest,
        is_owner: this.is_owner,
        has_hands_up: this.has_hands_up,
      }
    }
  },

  watch: {
    is_guest () {
      if (this.is_guest && undefined !== this.update_peer_list_interval_id)
      {
        clearInterval(this.update_peer_list_interval_id)
        this.update_peer_list_interval_id = undefined
      }
      if (!this.is_guest && undefined === this.update_peer_list_interval_id)
      {
        this.update_peer_list_interval_id = setInterval(this.update_peer_list, 10000)
      }
    },
  },


  mounted () {
    this.get_messages_interval_id = setInterval(this.get_messages, 2000)
    this.update_peer_list_interval_id = setInterval(this.update_peer_list, 10000)
  },

  beforeDestroy () {
    clearInterval(this.get_messages_interval_id)
    if (undefined !== this.update_peer_list_interval_id)
    {
      clearInterval(this.update_peer_list_interval_id)
    }
  },


  methods: {
    update_user_name () {
      let uname = Cookies.get('user_name')
      if (uname !== this.user_name)
      {
        this.user_name = uname
      }
    },

    on_get_messages_success (json) {
      if(json.messages.length > 0)
      {
        this.last_checked_out_message_id = json.messages[json.messages.length-1].id
        this.messages.splice(this.messages.length, 0, ...json.messages)
      }
      if(json.authorized_peers !== this.authorized_peers)
      {
        this.authorized_peers = json.authorized_peers
      }
      this.update_user_name()
    },

    get_messages () {
      if (this.last_checked_out_message_id < 0)
      {
        var url = 'get_messages'
      }
      else
      {
        var url = `get_messages/${encodeURIComponent(this.last_checked_out_message_id)}`
      }

      $.get({
        url : url,
        dataType: 'json',
        success : this.on_get_messages_success,
      })
    },

    on_update_peer_list_success (json) {
      this.waving_hand_peers = json.waving_hand
      this.other_peers = json.others
    },

    update_peer_list: function () {
      $.post({
        url : 'peer_list',
        dataType: 'json',
        success : this.on_update_peer_list_success,
      })
    },
  },

  template: `
    <div id="board_main">
      <message_board :messages="messages">
      </message_board>
      <the_panel
        :owner_name="owner_name"
        :user_info="user_info"
      ></the_panel>

      <div id="auxiliary_panel">
        <peer_list
          v-if="!is_guest"
          :user_info="user_info"
          :waving_hand="waving_hand_peers"
          :others="other_peers"
        ></peer_list>
        <user_info
          :user_name="user_name"
          :user_info="user_info"
        ></user_info>
      </div>
    </div>
  `
})
