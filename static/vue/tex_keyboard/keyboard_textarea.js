"use strict";

var keys_to_disallow = [33, 34, 38, 40]

var tex_tree = new TexRoot()
var tex_caret = new TexCaret(tex_tree)
function reset_tex_tree ()
{
  tex_tree = new TexRoot()
  tex_caret = new TexCaret(tex_tree)
}

Vue.component('tex_input', {
  props: {
    placeholder: String,
  },

  data: function () {
    return {
      textarea_value: '',
    }
  },

  computed: {
  },

  methods: {
    tex_preview_text: function () {
      return tex_tree.generate_preview(tex_caret)
    },

    refresh_textarea () {
      if (undefined === this.last_refresh)
      {
        this.last_refresh = 0
      }
      now = new Date().getTime()
      if (now - this.last_refresh > 200)
      {
        this.last_refresh = now
        this.textarea_value = tex_tree.generate_contents(tex_caret)
      }
      this.update_caret_position()
    },

    push_keyrow (keyrow_name) {
      tex_caret.push_user_keyrow(keyrow_name)
      this.update_recomended_keyrows()
    },

    update_recomended_keyrows () {
      // Where is the propper place to give focus to tex_area?
      let textarea = this.$refs.tex_area
      textarea.focus()

      let keyboard = this.$refs.keyboard
      keyboard.is_tex_mode = tex_caret.is_math_mode()

      if (keyboard.is_tex_mode)
      {
        keyboard.recomended_keyrows = tex_caret.recomended_keyrows_with_duplicates()
        keyboard.control_keyrow_choice = tex_caret.get_control_keyrow()
      }
    },

    update_caret_position () {
      let start = tex_caret.get_start_char()
      let end = tex_caret.get_end_char()
      let direction = tex_caret.get_selection_direction()

      let textarea = this.$refs.tex_area
      textarea.focus()

      this.$nextTick(function () {
        textarea.setSelectionRange(start, end, direction)
        this.update_preview()
      })

      this.update_recomended_keyrows()
    },

    update_preview () {
      if (undefined === this.last_preview)
      {
        this.last_preview = 0
        this.preview_timeout_id = undefined
      }
      now = new Date().getTime()
      if (now - this.last_preview < 500)
      {
        clearTimeout(this.preview_timeout_id)
        this.preview_timeout_id = setTimeout(this.update_preview.bind(this), 1500)
        return
      }
      this.last_preview = now
      preview_input(this.tex_preview_text())
    },

    insert_nodes (...nodes) {
      tex_caret.insert_nodes(...nodes)
      tex_caret.move_right()
      this.refresh_textarea()
    },

    move_left (shift) {
      if (!shift)
      {
        tex_caret.move_left()
        this.update_caret_position()
      }
      else
      {
        tex_caret.select_left()
        this.refresh_textarea()
      }
    },

    move_right (shift) {
      if (!shift)
      {
        tex_caret.move_right()
        this.update_caret_position()
      }
      else
      {
        tex_caret.select_right()
        this.refresh_textarea()
      }
    },

    change_block (style) {
      let block_node = tex_caret.get_context_block()
      block_node.set_style(style)
      this.refresh_textarea()
    },

    key_backspace () {
      if (tex_caret.has_selection())
      {
        tex_caret.remove_selection()
      }
      else
      {
        if (tex_caret.is_at_block_start()) {
          console.log('Will not backspace the block from inside.')
          return
        }
        tex_caret.select_left()
        if (!tex_caret.selection.start.is_block())
        {
          tex_caret.remove_selection()
        }
      }
      this.refresh_textarea()
    },

    key_delete () {
      if (tex_caret.has_selection())
      {
        tex_caret.remove_selection()
      }
      else
      {
        if (tex_caret.is_at_block_end()) {
          console.log('Will not delete the block from inside.')
          return
        }
        tex_caret.select_right()
        if (!tex_caret.selection.start.is_block())
        {
          tex_caret.remove_selection()
        }
      }
      this.refresh_textarea()
    },

    key_enter (shift) {
      if (!shift)
      {
        this.input_text('\n')
      }
      else
      {
        this.submit_tex()
      }
    },

    submit_tex () {
      this.$refs.tex_area.form.onsubmit()
      this.refresh_textarea()
    },

    do_block_setting (function_name) {
      let block_node = tex_caret.get_context_block()
      block_node[function_name](tex_caret)
      this.refresh_textarea()
    },

    input_text (text) {
      this.insert_nodes(new TexLeaf(text))
    },

    input_key (event) {
      if (event.isComposing)
      {
        return
      }
      this.input_text(event.data)
    },

    key_down (event) {
      let code = event.keyCode ? event.keyCode : event.which
      let shift = event.shiftKey
      if (37 == code) {
        // Left
        event.preventDefault()
        this.move_left(shift)
      } else if (39 == code) {
        // Right
        event.preventDefault()
        this.move_right(shift)
      } else if (8 == code) {
        // Backspace
        event.preventDefault()
        this.key_backspace()
      } else if (46 == code) {
        // Delete
        event.preventDefault()
        this.key_delete()
      } else if (13 == code) {
        // Enter
        event.preventDefault()
        this.key_enter(shift)
      } else if (16 == code) {
        // Shift
        this.$refs.keyboard.meta_keys.shift = true
      } else if (17 == code) {
        // Ctrl
        this.$refs.keyboard.meta_keys.ctrl = true
      } else if (18 == code) {
        // Alt
        this.$refs.keyboard.meta_keys.alt = true
      } else if (keys_to_disallow.includes(code)) {
        event.preventDefault()
      }
    },

    key_up (event) {
      let code = event.keyCode ? event.keyCode : event.which
      if (16 == code) {
        // Shift
        this.$refs.keyboard.meta_keys.shift = false
      } else if (17 == code) {
        // Ctrl
        this.$refs.keyboard.meta_keys.ctrl = false
      } else if (18 == code) {
        // Alt
        this.$refs.keyboard.meta_keys.alt = false
      }
    },
  },

  // TODO: textarea id and name should not be in the template.
  // HACK: tabindex so that div can get focus (CSS).
  // TODO: pass :layout_tree to first keyset (extra keys).
  template: `
    <div class="tex_input" tabindex="-1">
      <div class="input_and_button">
        <textarea ref="tex_area" class="tex_area"
          id="board_input" name="board_input"
          :placeholder="placeholder"
          :value="textarea_value"
          @input="input_key"
          @keydown="key_down"
          @keyup="key_up"
        ></textarea>
        <input type="submit" value="Enviar"
          @click.prevent="submit_tex"
          ref="submit"
        />
      </div>
      <tex_keyboard
        ref="keyboard"
        @change_block="change_block"
        @keydown="key_down"
        @keyup="key_up"
        @virtual_metakey="key_down"
        @input="input_key"
        @do_block_setting="do_block_setting"
        @push_keyrow="push_keyrow"
        @insert_nodes="insert_nodes"></tex_keyboard>
    </div>
  `
})
