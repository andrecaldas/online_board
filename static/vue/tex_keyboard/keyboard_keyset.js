"use strict";

Vue.component('tex_key', {
  props: {
    key_description: {type: Object, required: true},

    shift: {type: Boolean, required: true},
    ctrl: {type: Boolean, required: true},
    alt: {type: Boolean, required: true},
  },

  data () { return {
    css_classes: this.key_description.css_classes(),
  }},

  computed: {
    display_value () {
      // I guess Mathjax rendering of button contents
      // confuses Vue. So, cargo-culturing again... <span></span>
      return '<span>' + this.key_description.display_value(this.shift, this.ctrl, this.alt) + '</span>'
    },

    computed_classes () {
      let result = []
      if (this.shift) result.push(this.css_classes.shift)
      if (this.ctrl) result.push(this.css_classes.ctrl)
      if (this.alt) result.push(this.css_classes.alt)
      return result
    },
  },

  methods: {
    click (event) {
      this.key_description.on_click(this)
    },
  },

  template: `
    <button
      :class="computed_classes"
      @click.prevent="click"
      v-html="display_value"
    ></button>
  `
})


Vue.component('tex_keyrow', {
  props: {
    shift: {type: Boolean, required: true},
    ctrl: {type: Boolean, required: true},
    alt: {type: Boolean, required: true},

    layout_tree: {type: Array, required: true},
  },

  methods: {
    render_mathjax () {
      // By some cargo-culture reason, I belive Vue is waiting for MathJax promise.
      // This is a cargo-culture hack to avoid it. :-)
      setTimeout(() => {
        MathJax.typesetPromise([this.$el])
      })
    },
  },

  watch: {
    shift () {this.$nextTick(() => this.render_mathjax())},
    ctrl () {this.$nextTick(() => this.render_mathjax())},
    alt () {this.$nextTick(() => this.render_mathjax())},
  },

  mounted () {
    this.render_mathjax()
  },

  // TODO: What to use as :key for inner most tex_key?
  template: `
    <div>
      <tex_key v-for="(key,i) in layout_tree"
        :key_description="key"
        :shift="shift" :ctrl="ctrl" :alt="alt"
        :key="i"
        v-on="$listeners">
      </tex_key>
    </div>
  `
})


Vue.component('tex_keyset', {
  props: {
    shift: {type: Boolean, required: true},
    ctrl: {type: Boolean, required: true},
    alt: {type: Boolean, required: true},

    layout_tree: {type: Object, required: true},
  },

  data () { return {
  }},

  template: `
    <div>
      <div
        v-for="(keys,name) in layout_tree"
        :class="['key_group', 'key_group' + name]">
          <tex_keyrow :layout_tree="keys"
            :shift="shift" :ctrl="ctrl" :alt="alt"
            v-on="$listeners">
          </tex_keyrow>
      </div>
    </div>
  `
})


Vue.component('tex_keyboard', {
  data () { return {
    meta_keys: {
      shift: false,
      ctrl: false,
      alt: false,
    },

    active_keyrow_trees: Object.freeze(tex_keyrows),
    tex_keyrow_selectors: Object.freeze(tex_keyrow_selectors),
    tex_block_control: Object.freeze(tex_block_control),
    tex_blocks: Object.freeze(tex_blocks),
    tex_navigation: Object.freeze(tex_navigation),
    qwerty_keyset: Object.freeze(qwerty_keyset),

    recomended_keyrows: [],
    control_keyrow_choice: undefined,
    is_tex_mode: false,
  }},

  computed: {
    active_keyrow_order () {
      let result = {}
      for (let i in this.active_keyrow_list)
      {
        result[this.active_keyrow_list[i]] = 'order_' + i
      }
      return result
    },

    active_keyrow_list () {
      let list = Object.keys(tex_keyrows)
      let concat = this.recomended_keyrows.concat(list)
      let result = concat.reduce((acc, cur) => {if (acc.includes(cur)) return acc; else return acc.concat(cur)}, [])
      return result
    },
  },

  methods: {
    virtual_metakey (event) {
console.log('Virtual meta key:', event)
      if (event instanceof FakeKeyEvent)
      {
        this.$emit('virtual_metakey', event)
        return
      }
      this.meta_keys[event] = this.meta_keys[event] ? false : true
console.log('Meta keys:', this.meta_keys)
    },
  },

  template: `
    <div v-if="is_tex_mode" class="tex_keyboard">

      <!-- Each block has a control keyrow. Those are shown here. -->
      <tex_keyrow
        v-if="control_keyrow_choice"
        :layout_tree="tex_block_control[control_keyrow_choice]"
        :key="control_keyrow_choice"
        class="tex_block_control"
        :shift="meta_keys.shift" :ctrl="meta_keys.ctrl" :alt="meta_keys.alt"
        @virtual_metakey="virtual_metakey"
        v-on="$listeners"
      ></tex_keyrow>

      <div class="non_control_block">
        <!-- Fixed: arrows/shift/ctrl/alt and block tex commands. -->
        <div class="tex_fixed_buttons">
          <!-- Block tex commands. -->
          <tex_keyrow
            :layout_tree="tex_blocks"
            class="tex_blocks"
            :shift="meta_keys.shift" :ctrl="meta_keys.ctrl" :alt="meta_keys.alt"
            @virtual_metakey="virtual_metakey"
            v-on="$listeners"
          ></tex_keyrow>
          <!-- Fixed: arrows/shift/ctrl/alt. -->
          <div
            v-for="(row,i) in tex_navigation"
            :key="i"
            class="tex_navigation">
            <tex_keyrow
              :layout_tree="row"
              :shift="meta_keys.shift" :ctrl="meta_keys.ctrl" :alt="meta_keys.alt"
              @virtual_metakey="virtual_metakey"
              v-on="$listeners"
            ></tex_keyrow>
          </div>
        </div>
        <div class="tex_active_keys">
          <tex_keyrow
            v-for="(keyrow, name) in active_keyrow_trees"
            :layout_tree="keyrow"
            :key="name"
            :class="[name, active_keyrow_order[name]]"
            :shift="meta_keys.shift" :ctrl="meta_keys.ctrl" :alt="meta_keys.alt"
            @virtual_metakey="virtual_metakey"
            v-on="$listeners"
          ></tex_keyrow>
        </div>
      </div>

      <!-- Keys to select the active keyrow -->
      <tex_keyrow
        :layout_tree="tex_keyrow_selectors"
        :shift="meta_keys.shift" :ctrl="meta_keys.ctrl" :alt="meta_keys.alt"
        @virtual_metakey="virtual_metakey"
        v-on="$listeners"
      ></tex_keyrow>
    </div>
    <div v-else class="tex_keyboard">
      <tex_keyset :layout_tree="qwerty_keyset"
        :shift="meta_keys.shift" :ctrl="meta_keys.ctrl" :alt="meta_keys.alt"
        @virtual_metakey="virtual_metakey"
        v-on="$listeners"
      ></tex_keyset>
    </div>
  `
})
