"use strict";

class KeyDescription {
  constructor (val, shift_val = undefined) {
    this.val = val
    this.d_val = val
    this.shift_val = shift_val
    this.d_shift_val = shift_val
    this.ctrl_val = undefined
    this.d_ctrl_val = undefined
    this.alt_val = undefined
    this.d_alt_val = undefined
    this.ctrl_shift_val = undefined
    this.d_ctrl_shift_val = undefined
    this.ctrl_alt_val = undefined
    this.d_ctrl_alt_val = undefined
    this.alt_shift_val = undefined
    this.d_alt_shift_val = undefined
    this.ctrl_alt_shift_val = undefined
    this.d_ctrl_alt_shift_val = undefined
    this.NodeClass = TexLeaf
  }

  on_click (vue_component) {
    let value = this.value(vue_component.shift, vue_component.ctrl, vue_component.alt)
    vue_component.$emit('insert_nodes', new this.NodeClass(value))
  }

  display_val_array () {
    return [
      this.d_val,
      this.d_shift_val || this.d_val,
      this.d_ctrl_val || this.d_val,
      this.d_ctrl_shift_val || this.d_ctrl_val || this.d_shift_val || this.d_val,
      this.d_alt_val || this.d_val,
      this.d_alt_shift_val || this.d_alt_val || this.d_shift_val || this.d_val,
      this.d_ctrl_alt_val || this.d_shift_val || this.d_val,
      this.d_ctrl_alt_shift_val || this.d_ctrl_alt_val || this.d_shift_val || this.d_val,
    ]
  }

  press_val_array () {
    return [
      this.val,
      this.shift_val || this.val,
      this.ctrl_val || '',
      this.ctrl_shift_val || this.ctrl_val || '',
      this.alt_val || '',
      this.alt_shift_val || this.alt_val || '',
      this.ctrl_alt_val || '',
      this.ctrl_alt_shift_val || this.ctrl_alt_val || '',
    ]
  }

  value (shift, ctrl, alt) {
    let s = (shift)?1:0
    let c = (ctrl)?2:0
    let a = (alt)?4:0

    return this.press_val_array()[s + c + a]
  }

  display_value (shift, ctrl, alt) {
    let s = (shift)?1:0
    let c = (ctrl)?2:0
    let a = (alt)?4:0

    return this.display_val_array()[s + c + a]
  }

  css_classes () {
    return {shift: '', ctrl: '', alt: ''}
  }
}

let default_keys = {}
let a_code = 'a'.charCodeAt(0)
let A_code = 'A'.charCodeAt(0)
for (let i=0; i < 26; ++i) {
  let x = String.fromCharCode(a_code + i)
  let X = String.fromCharCode(A_code + i)
  default_keys[x] = new KeyDescription(x, X)
}

default_keys['1'] = new KeyDescription('1', '!')
default_keys['2'] = new KeyDescription('2', '@')
default_keys['3'] = new KeyDescription('3', '#')
default_keys['4'] = new KeyDescription('4', '$')
default_keys['5'] = new KeyDescription('5', '%')
default_keys['6'] = new KeyDescription('6', '"')
default_keys['7'] = new KeyDescription('7', '&')
default_keys['8'] = new KeyDescription('8', '*')
default_keys['9'] = new KeyDescription('9', '(')
default_keys['0'] = new KeyDescription('0', ')')
default_keys['-'] = new KeyDescription('-', '_')
default_keys['='] = new KeyDescription('=', '+')

default_keys['space'] = new KeyDescription(' ', ' ')
default_keys['space'].d_val = 'space'
default_keys['space'].d_shift_val = 'space'


class FakeKeyEvent {
  constructor (key_name) {
    this.key_name = ''
  }

  preventDefault () {}
}

class MetaKeyDescription extends KeyDescription {
  constructor (label, event) {
    super(undefined, undefined)
    this.d_val = label
    this.event = event
  }

  on_click (vue_component) {
    console.log('Fake keydown...', this.event)
    vue_component.$emit('virtual_metakey', this.event)
  }

  css_classes () {
    let result = super.css_classes()
    result[this.event] = 'key_down'
    return result
  }
}


// TODO: special meta keys.
default_keys['shift'] = new MetaKeyDescription('shift', 'shift')
default_keys['ctrl'] = new MetaKeyDescription('ctrl', 'ctrl')
default_keys['alt'] = new MetaKeyDescription('alt', 'alt')
default_keys['left'] = new MetaKeyDescription('<', new FakeKeyEvent(37))
default_keys['right'] = new MetaKeyDescription('>', new FakeKeyEvent(39))


/*
* Some keys that might not be in querty (need shift).
*/
let ekeys = [
  '+', '*', '-', '/', '\\', ',', '.', '',
  '(', ')', '[', ']',
]
for (let k of ekeys)
{
  if (undefined === default_keys[k])
  {
    default_keys[k] = new KeyDescription(k, undefined)
  }
}



class TexBlockControlDescription extends KeyDescription {
  constructor (label, block_style) {
    super(undefined, undefined)
    this.d_val = label
    this.block_style = block_style
  }

  on_click (vue_component) {
    vue_component.$emit('change_block', this.block_style)
  }
}

class TexBlockCommandDescription extends KeyDescription {
  constructor (label, block_method) {
    super(undefined, undefined)
    this.d_val = label
    this.block_method =  block_method
  }

  on_click (vue_component) {
    vue_component.$emit('do_block_setting', this.block_method)
  }
}

class TexBlockDescription extends KeyDescription {
  constructor (label, NodeClass, shift_label = undefined, shift_NodeClass = undefined) {
    super(undefined, undefined)
    this.d_val = label
    this.NodeClass = NodeClass
    console.assert(undefined === shift_label)
  }
}

class TexLetterDescription extends KeyDescription {
  constructor (
    text,
    shift_text = undefined,
    ctrl_text = undefined,
    ctrl_shift_text = undefined,
    alt_text = undefined
  ) {
    super(text, shift_text)
    this.d_val = text ? '$' + text + '$' : undefined
    this.d_shift_val = '$' + (shift_text?shift_text:text) + '$'
    this.ctrl_val = ctrl_text
    this.d_ctrl_val = ctrl_text ? '$' + ctrl_text + '$' : undefined
    this.ctrl_shift_val = ctrl_shift_text
    this.d_ctrl_shift_val = ctrl_shift_text ? '$' + ctrl_shift_text + '$' : undefined
    this.alt_val = alt_text
    this.d_alt_val = alt_text ? '$' + alt_text + '$' : undefined
  }
}

class ActivateRowDescription extends KeyDescription {
  constructor (label, keyrow_name) {
    super(undefined, undefined)
    this.d_val = label
    this.keyrow_name = keyrow_name
  }

  on_click (vue_component) {
    vue_component.$emit('push_keyrow', this.keyrow_name)
  }
}



// MathJax doesn't have italic upper case Greek letters.
// https://meta.stackexchange.com/questions/328365/is-it-possible-to-write-upright-greek-letters-on-stack-exchange-in-math-mode
default_keys['ma'] = new TexLetterDescription('a', 'A', '{\\alpha}', '{\\rm\\Alpha}')
default_keys['mb'] = new TexLetterDescription('b', 'B', '{\\beta}', '{\\rm\\Beta}')
default_keys['mc'] = new TexLetterDescription('c', 'C', '{\\gamma}', '{\\rm\\Gamma}')
default_keys['md'] = new TexLetterDescription('d', 'D', '{\\delta}', '{\\rm\\Delta}', '{\\partial}', '{\\nabla}')
default_keys['me'] = new TexLetterDescription('e', 'E', '{\\eta}', '{\\rm\\Eta}', '{\\mathrm{e}}')
default_keys['mx'] = new TexLetterDescription('x', 'X', '{\\chi}', '{\\rm\\Chi}')
default_keys['my'] = new TexLetterDescription('y', 'Y', '{\\upsilon}', '{\\rm\\Upsilon}')
default_keys['mz'] = new TexLetterDescription('z', 'Z', '{\\zeta}', '{\\rm\\Zeta}')
default_keys['mf'] = new TexLetterDescription('f', 'F', '{\\varphi}', '{\\rm\\Phi}')
default_keys['mg'] = new TexLetterDescription('g', 'G', '{\\gamma}', '{\\rm\\Gamma}')
default_keys['mh'] = new TexLetterDescription('h', 'H', undefined, undefined, '{\\hbar}')
default_keys['mi'] = new TexLetterDescription('i', 'I', '{\\iota}', '{\\rm\\Iota}')
default_keys['mj'] = new TexLetterDescription('j', 'J', undefined, undefined, '{\\jmath}')
default_keys['mk'] = new TexLetterDescription('k', 'K', '{\\kappa}', '{\\rm\\Kappa}')
default_keys['ml'] = new TexLetterDescription('l', 'L', '{\\lambda}', '{\\Lambda}', '{\\ell}')
default_keys['m_x_'] = new TexLetterDescription('(x)', '(a)', '(\\alpha)')
default_keys['m_y_'] = new TexLetterDescription('(y)', '(b)', '(\\beta)')
default_keys['m_z_'] = new TexLetterDescription('(z)', '(c)', '(\\gamma)')
default_keys['m_xy_'] = new TexLetterDescription('(x,y)', '(a,b)', '(\\alpha, \\beta)')
default_keys['m_xyz_'] = new TexLetterDescription('(x,y,z)', '(a,b,c)', '(\\alpha, \\beta, \\gamma)')
default_keys['times'] = new TexLetterDescription('{\\times}')
default_keys['vec_a'] = new TexLetterDescription('{\\vec{a}}', '{\\vec{u}}')
default_keys['vec_b'] = new TexLetterDescription('{\\vec{b}}', '{\\vec{v}}')
default_keys['vec_c'] = new TexLetterDescription('{\\vec{c}}', '{\\vec{w}}')
default_keys['cup'] = new TexLetterDescription('{\\cup}')
default_keys['cap'] = new TexLetterDescription('{\\cap}')
default_keys['oplus'] = new TexLetterDescription('{\\oplus}')
default_keys['otimes'] = new TexLetterDescription('{\\otimes}')
default_keys['wedge'] = new TexLetterDescription('{\\wedge}')
default_keys['vee'] = new TexLetterDescription('{\\vee}')


default_keys['formula'] = new TexBlockDescription('fórmula', TexFormula)
default_keys['integral'] = new TexBlockDescription('$\\int \\square$', TexIntegral)
default_keys['matrix'] = new TexBlockDescription('$\\left[M\\right]$', TexMatrix)
default_keys['big_operator'] = new TexBlockDescription('$\\sum, \\prod, \\dotsc$', TexBigOperator)
default_keys['set'] = new TexBlockDescription('$\\{\\square :\\, \\square\\}$', TexSet)

default_keys['frac'] = new TexBlockDescription('$\\frac{\\square}{\\square}$', TexFrac)
default_keys['pow'] = new TexBlockDescription('${\\square}^{\\square}$', TexPow)
default_keys['sqrt'] = new TexBlockDescription('$\\sqrt{\\square}$', TexSqrt)


function compile_row (row) {
  console.log('Compiling row', row)
  console.assert(-1 === row.trim().split(/\s+/).map((key) => default_keys[key]).indexOf(undefined), 'Undefined key')
  return Object.freeze(row.trim().split(/\s+/).map((key) => default_keys[key]))
}


var tex_keyrows = {
  numbers: compile_row('1 2 3 4 5 6 7 8 9 0 , .'),
  operations: compile_row('+ - times frac = pow sqrt'),
  variables: compile_row('mx my mz ma mb mc mi mj mk'),
  functions: compile_row('mf mg mh m_x_ m_y_ m_z_ m_xy_ m_xyz_'),
  vectors: compile_row('vec_a vec_b vec_c'),
  sets: compile_row('cup cap oplus otimes wedge vee'),
}

let selectors = [
  ['numbers', 'Números'],
  ['operations', 'Operações'],
  ['variables', 'Variáveis'],
  ['functions', 'Funções'],
  ['vectors', 'Vetores'],
  ['sets', 'Conjuntos'],
]
var tex_keyrow_selectors = []
for (let s of selectors)
{
  let key_name = 'select_' + s[0]
  default_keys[key_name] = new ActivateRowDescription(s[1], s[0])
  tex_keyrow_selectors.push(default_keys[key_name])
}


var tex_blocks = compile_row('integral matrix big_operator set')
var tex_navigation = [
  compile_row('left shift right'),
  compile_row('ctrl alt'),
]

var qwerty_keyset = {
  l1: compile_row('q w e r t y u i o p'),
  l2: compile_row('a s d f g h j k l'),
  l3: compile_row('shift z x c v b n m shift'),
  l4: compile_row('ctrl alt space formula'),
}

default_keys['formula_block'] = new TexBlockControlDescription('Em bloco', 'block')
default_keys['formula_inline'] = new TexBlockControlDescription('Em linha', 'inline')

default_keys['bo_sum'] = new TexBlockControlDescription('$\\sum$', 'sum')
default_keys['bo_prod'] = new TexBlockControlDescription('$\\prod$', 'prod')
default_keys['bo_cup'] = new TexBlockControlDescription('$\\bigcup$', 'cup')
default_keys['bo_cap'] = new TexBlockControlDescription('$\\bigcap$', 'cap')
default_keys['bo_oplus'] = new TexBlockControlDescription('$\\bigoplus$', 'oplus')
default_keys['bo_otimes'] = new TexBlockControlDescription('$\\bigotimes$', 'otimes')
default_keys['bo_wedge'] = new TexBlockControlDescription('$\\bigwedge$', 'wedge')
default_keys['bo_vee'] = new TexBlockControlDescription('$\\bigvee$', 'vee')
default_keys['bo_no_limits'] = new TexBlockCommandDescription('$\\phantom{\\sum}_{\\boxtimes}^{\\boxtimes}$', 'no_limits')
default_keys['bo_only_lower'] = new TexBlockCommandDescription('$\\phantom{\\sum}_{\\square}^{\\boxtimes}$', 'only_lower')
default_keys['bo_lower_and_upper'] = new TexBlockCommandDescription('$\\phantom{\\sum}_{\\square}^{\\square}$', 'lower_and_upper')

default_keys['matrix_none'] = new TexBlockControlDescription('${\\small\\color{darkgray}(}\\square{\\small\\color{darkgray})}$', 'none')
default_keys['matrix_parenthesis'] = new TexBlockControlDescription('$(\\square)$', 'pmatrix')
default_keys['matrix_brackets'] = new TexBlockControlDescription('$[\\square]$', 'bmatrix')
default_keys['matrix_curly'] = new TexBlockControlDescription('$\\{\\square\\}$', 'Bmatrix')
default_keys['matrix_vertical'] = new TexBlockControlDescription('$|\\square|$', 'vmatrix')
default_keys['matrix_double'] = new TexBlockControlDescription('$\\begin{Vmatrix}\\square\\end{Vmatrix}$', 'Vmatrix')
default_keys['matrix_+col'] = new TexBlockCommandDescription('$+$ cols', 'add_column')
default_keys['matrix_-col'] = new TexBlockCommandDescription('$-$ cols', 'del_column')
default_keys['matrix_+line'] = new TexBlockCommandDescription('$+$ line', 'add_line')
default_keys['matrix_-line'] = new TexBlockCommandDescription('$-$ line', 'del_line')

default_keys['integral_int'] = new TexBlockControlDescription('$\\int$', 'int')
default_keys['integral_iint'] = new TexBlockControlDescription('$\\iint$', 'iint')
default_keys['integral_iiint'] = new TexBlockControlDescription('$\\iiint$', 'iiint')
default_keys['integral_oint'] = new TexBlockControlDescription('$\\oint$', 'oint')
default_keys['integral_oiint'] = new TexBlockControlDescription('$\\oiint$', 'oiint')
default_keys['integral_oiiint'] = new TexBlockControlDescription('$\\oiiint$', 'oiiint')

default_keys['integral_no_limits'] = new TexBlockCommandDescription('$\\int_{\\boxtimes}^{\\boxtimes}$', 'no_limits')
default_keys['integral_only_lower'] = new TexBlockCommandDescription('$\\int_{\\square}^{\\boxtimes}$', 'only_lower')
default_keys['integral_lower_and_upper'] = new TexBlockCommandDescription('$\\int_{\\square}^{\\square}$', 'lower_and_upper')
default_keys['integral_add_differential'] = new TexBlockCommandDescription('$+\\mathrm{d}\\square$', 'add_differential')
default_keys['integral_remove_differential'] = new TexBlockCommandDescription('$-\\mathrm{d}\\square$', 'remove_differential')

default_keys['set_condition'] = new TexBlockCommandDescription('$\\{\\square :\\, \\square\\}$', 'set_condition')
default_keys['set_no_condition'] = new TexBlockCommandDescription('$\\{\\square \\cancel{:\\, \\square}\\}$', 'remove_condition')

default_keys['block_parenthesis'] = new TexBlockCommandDescription('$(\\ldots)$', 'block_parenthesis')
default_keys['block_no_parenthesis'] = new TexBlockCommandDescription('${\\small\\color{darkgray}(}\\ldots{\\small\\color{darkgray})}$', 'block_no_parenthesis')

default_keys['pow_parenthesis'] = new TexBlockCommandDescription('$({\\square})^{\\square}$', 'main_part_parenthesis')
default_keys['pow_no_parenthesis'] = new TexBlockCommandDescription('${\\square}^{\\square}$', 'main_part_no_parenthesis')

default_keys['sqrt_no_exponent'] = new TexBlockCommandDescription('$\\sqrt[\\boxtimes]{\\square}$', 'remove_exponent')
default_keys['sqrt_2'] = new TexBlockCommandDescription('$\\sqrt[2]{\\square}$', 'set_exponent_two')
default_keys['sqrt_3'] = new TexBlockCommandDescription('$\\sqrt[3]{\\square}$', 'set_exponent_three')
default_keys['sqrt_set_exponent'] = new TexBlockCommandDescription('$\\sqrt[\\square]{\\square}$', 'set_exponent')


var tex_block_control = {
  formula: compile_row('formula_block formula_inline'),
  integral: compile_row(
    'integral_int integral_iint integral_iiint integral_oint' +
    ' integral_no_limits integral_only_lower integral_lower_and_upper integral_add_differential integral_remove_differential'
  ),
  matrix: compile_row('matrix_none matrix_parenthesis matrix_brackets matrix_curly matrix_vertical matrix_double matrix_+col matrix_+line matrix_-col matrix_-line'),
  big_operator: compile_row('bo_no_limits bo_only_lower bo_lower_and_upper bo_sum bo_prod bo_cup bo_cap bo_oplus bo_otimes bo_wedge bo_vee'),
  sets: compile_row('set_condition set_no_condition'),
  frac: compile_row('block_parenthesis block_no_parenthesis'),
  pow: compile_row('pow_parenthesis pow_no_parenthesis'),
  sqrt: compile_row('sqrt_no_exponent sqrt_2 sqrt_3 sqrt_set_exponent'),
}
