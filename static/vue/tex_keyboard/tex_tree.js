'use strict';

class TexNode {
  constructor () {
    this.ignore = false
  }
}


class TexLeaf extends TexNode {
  constructor (contents) {
    super()
    this.contents = contents
  }

  char_count () {
    return this.contents.length
  }

  generate_contents (caret, result) {
    if (this.ignore) { return }
    result.append_text(this.contents)
  }

  generate_preview (caret, result, before_text = '', after_text = '') {
    if (caret.has_selection() && this === caret.selection.start)
    {
      result.append_text('{\\class{selection_math}{', '<span class="selection_math">')
    }

    if (!this.ignore)
    {
      result.append_text(before_text)
      result.append_text(escape_html(this.contents))
      result.append_text(after_text)
    }

    if (caret.has_selection() && this === caret.selection.end)
    {
      result.append_text('}}', '</span>')
    }

    if (this === caret.caret_node)
    {
      // Atention: TexFormulaStart and TexFormulaEnd
      // need to set result.is_math_mode before calling generate_preview()
      result.append_text('{\\class{caret_math}{|}}', '<span class="caret_math">|</span>')
    }
  }

  is_block () {
    return false
  }
}


class TexBlock extends TexNode {
  constructor (opening_text, closing_text, styles = {}) {
    super()
    let opening = (opening_text instanceof TexLeaf) ? opening_text : new TexLeaf(opening_text)
    let closing = (closing_text instanceof TexLeaf) ? closing_text : new TexLeaf(closing_text)
    this.nodes = [opening, closing]
    this._is_subordinated_block = false
    this.is_math_mode = undefined
    this.caret_forbidden = []
    this.recomended_keyrows = []
    this.user_choosen_keyrows = []
    // If control_keyrow is undefined, use parent block's control.
    // Caret is responsible for finding the innermost block
    // with control_keyrow !== undefined.
    this.control_keyrow = undefined
    this.styles = styles
  }

  is_block () {
    return true
  }

  set_style(style) {
    this.nodes[0].contents = this.styles[style][0]
    this.nodes[this.nodes.length-1].contents = this.styles[style][1]
  }

  is_empty () {
    console.assert(this.nodes.length >=2)
    return (this.nodes.length <= 2)
  }

  is_node_allowed_front (node) {
    if (this.ignore) { return false }
    console.assert(node !== this.nodes[this.nodes.length-1], 'Do not test last node.')
    let i = this.nodes.indexOf(node)
    console.assert(i !== -1)
    return this.is_node_pair_allowed(node, this.nodes[i+1])
  }

  is_node_allowed_back (node) {
    if (this.ignore) { return false }
    console.assert(node !== this.nodes[0], 'Do not test first node.')
    let i = this.nodes.indexOf(node)
    console.assert(i !== -1)
    return this.is_node_pair_allowed(this.nodes[i-1], node)
  }

  is_node_pair_allowed (n1, n2) {
    return (undefined === this.caret_forbidden.find(f => (f[0] === n1 && f[1] === n2)))
  }

  prev (node) {
    let i = this.nodes.indexOf(node)
    console.assert(i > 0)
    return this.nodes[i - 1]
  }

  next (node) {
    let index = this.nodes.indexOf(node)
    console.assert(index >= 0)
    console.assert(undefined !== this.nodes[index+1])
    return this.nodes[index + 1]
  }

  start (i = 0) {
    return this.nodes[0 + i]
  }

  end (i = 0) {
    console.assert(i <= 0)
    return this.nodes[this.nodes.length - 1 + i]
  }

  is_start (node, i = 0) {
    return (node === this.start(i))
  }

  is_end (node, i = 0) {
    return (node === this.end(i))
  }

  paste_selection_on_creation (nodes) {
    this.insert_nodes_at_index(1, ...nodes)
    return [this]
  }

  insert_nodes_at_index (index, ...nodes) {
    console.assert(index < this.nodes.length, 'Inserting after last node? Probably a bug.')
    this.nodes.splice(index, 0, ...nodes)
  }

  append_nodes (...nodes) {
    this.insert_nodes_at_index(-1, ...nodes)
  }

  remove_node (node_or_index) {
    let i = node_or_index
    if (node_or_index instanceof TexNode)
    {
      i = this.nodes.indexOf(node_or_index)
      if (-1 === i)
      {
        console.log('Node for removal not found.')
        return undefined
      }
    }

    if (i < 0)
    {
      i = this.nodes.length + i
    }

    let left = this.nodes[i-1]
    let right = this.nodes[i+1]
    let removed_nodes = this.nodes.splice(i, 1)
    return {left: left, removed_nodes: removed_nodes, right: right}
  }

  generate_contents (caret, result) {
    if (this.ignore) { return }

    const caret_in_this_block = (this === caret.block_chain[0])
    const selection_in_this_block = (this === caret.block_chain[0] && undefined !== caret.selection)

    for (let child of this.nodes)
    {
      if (selection_in_this_block && child === caret.selection.start)
      {
        result.is_before_start = false
      }

      child.generate_contents(caret, result)

      if (selection_in_this_block && child === caret.selection.end)
      {
        result.is_before_end = false
      }

      if (caret_in_this_block && caret.caret_node === child)
      {
        result.is_before_caret = false
      }
    }
  }

  generate_preview_inner (caret, result) {
    // TODO: use is_start()
    if (this.is_empty() && this.nodes[0] !== caret.caret_node)
    {
      console.assert(2 === this.nodes.length)
      this.nodes[0].generate_preview(caret, result)
      result.append_text('{\\class{empty_math}{\\square}}', '')
      this.nodes[1].generate_preview(caret, result)
    }
    else
    {
      for (let node of this.nodes)
      {
        node.generate_preview(caret, result)
      }
    }
  }

  generate_preview (caret, result) {
    if (caret.has_selection() && this === caret.selection.start)
    {
      result.append_text('{\\class{selection_math}{', '<span class="selection_math">')
    }

    if (this === caret.block_chain[0])
    {
      result.append_text('{\\class{caret_block_math}{', '<span class="caret_block_math">')
    }

    if (!this.ignore)
    {
      this.generate_preview_inner(caret, result)
    }

    if (this === caret.block_chain[0])
    {
      result.append_text('}}', '</span>')
    }

    if (caret.has_selection() && this === caret.selection.end)
    {
      result.append_text('}}', '</span>')
    }

    if (this === caret.caret_node)
    {
      result.append_text('{\\class{caret_math}{|}}', '<span class="caret_math">|</span>')
    }
  }
}

class TexSubordinatedBlock extends TexBlock {
  constructor (left, right) {
    super(left, right)
    this._is_subordinated_block = true
  }
}


class TexNonEditableBlock extends TexBlock {
  is_node_allowed_front (caret) {
    return false
  }

  is_node_allowed_back (caret) {
    return false
  }

  paste_selection_on_creation (nodes) {
    assert(false, 'You need to implement this!')
    throw 'You need to implement this!'
  }
}

class TexRoot extends TexBlock {
  constructor () {
    super('', '')
    this.is_math_mode = false
  }

  generate_contents (caret) {
    console.assert(!this.ignore)
    let workpad = new ContentGenerationWorkpad()
    super.generate_contents(caret, workpad)
    caret.generation_result = workpad
    return workpad.text
  }

  generate_preview (caret) {
    console.assert(!this.ignore)
    let workpad = new PreviewGenerationWorkpad()
    super.generate_preview(caret, workpad)
//    console.log('Generated preview:', workpad.text)
    return workpad.text
  }
}

class PreviewGenerationWorkpad {
  constructor () {
    this.text = ''

    this.is_math_mode = false
  }

  append_text (text_or_math, non_math = undefined) {
    if (undefined === non_math)
    {
      non_math = text_or_math
    }
    this.text += (this.is_math_mode) ? text_or_math : non_math
  }
}

class ContentGenerationWorkpad {
  constructor () {
    this.text = ''

    // Used when generating contents.
    this.is_before_caret = true
    this.is_before_start = true
    this.is_before_end = true

    this.before_caret = 0
    this.before_start = 0
    this.before_end = 0
  }

  move_to_begin () {
    this.before_caret = 0
    this.before_start = 0
    this.before_end = 0
  }

  move_to_end () {
    this.before_caret = this.text.length - 1
    this.before_start = this.text.length - 1
    this.before_end = this.text.length - 1
  }

  unselect_left () {
    this.before_caret = this.before_start
    this.before_end = this.before_start
  }

  unselect_right () {
    this.before_caret = this.before_end
    this.before_start = this.before_end
  }

  append_text (text) {
    this.text += text
    if (this.is_before_caret) this.before_caret += text.length
    if (this.is_before_start) this.before_start += text.length
    if (this.is_before_end) this.before_end += text.length
  }

  move_left (char_count) {
    this.before_caret -= char_count
  }

  move_right (char_count) {
    this.before_caret += char_count
  }
}

let dumb_caret = {
  fix_removed () {},
}

class TexCaret {
  constructor (block, left_node = undefined) {
    this.root = block
    this.block_chain = [block]
    this.caret_node = (undefined === left_node) ? block.nodes[0] : left_node
    this.selection = undefined

    // TODO: initialize properly.
    this.generation_result = new ContentGenerationWorkpad()
  }

  next (node) {
    return this.block_chain[0].next(node)
  }

  prev (node) {
    return this.block_chain[0].prev(node)
  }

  is_at_block_start () {
    return (this.block_chain[0].is_start(this.caret_node))
  }

  is_at_block_end () {
    return (this.block_chain[0].is_end(this.caret_node, -1))
  }

  get_context_block () {
    for (let block of this.block_chain)
    {
      if (!block._is_subordinated_block)
      {
        return block
      }
    }
    console.assert(false)
  }

  get_start_char () {
    console.assert(undefined !== this.generation_result)
    if (undefined === this.selection)
    {
      return this.generation_result.before_caret
    }
    return this.generation_result.before_start
  }

  get_end_char () {
    console.assert(undefined !== this.generation_result)
    if (undefined === this.selection)
    {
      return this.generation_result.before_caret
    }
    return this.generation_result.before_end
  }

  get_selection_direction () {
    return (undefined === this.selection) ? 0 : this.selection.direction
  }

  move_left () {
    if (undefined !== this.selection)
    {
      this.caret_node = this.prev(this.selection.start)
      this.selection = undefined
      this.generation_result.unselect_left()
      return
    }

    if (this.block_chain.length === 1 && this.block_chain[0].is_start(this.caret_node))
    {
      return
    }

    if (this.caret_node.is_block())
    {
      this.block_chain.unshift(this.caret_node)
      console.assert(this.block_chain[0].nodes.length >= 2)
      this.caret_node = this.block_chain[0].nodes[this.block_chain[0].nodes.length-2]
      var char_count = this.block_chain[0].nodes[this.block_chain[0].nodes.length-1].char_count()
    }
    else
    {
      var char_count = this.caret_node.char_count()
      if (!this.block_chain[0].is_start(this.caret_node))
      {
        this.caret_node = this.block_chain[0].prev(this.caret_node)
      }
      else
      {
        console.assert(this.block_chain.length > 1)
        let popped_block = this.block_chain.shift()
        let i = this.block_chain[0].nodes.indexOf(popped_block)
        console.assert(i > 0)
        this.caret_node = this.block_chain[0].nodes[i-1]
      }
    }

    console.assert(undefined !== this.generation_result)
    this.generation_result.move_left(char_count)

    if (!this.block_chain[0].is_node_allowed_front(this.caret_node))
    {
      this.move_left()
    }
  }

  move_right () {
    if (undefined !== this.selection)
    {
      this.caret_node = this.selection.end
      this.selection = undefined
      this.generation_result.unselect_right()
      // DO return. Selection end is exactly where we want to be.
      return
    }

    if (this.block_chain.length === 1 && this.block_chain[0].is_end(this.caret_node, -1))
    {
      return
    }

    if (this.block_chain[0].is_end(this.caret_node, -1))
    {
      console.assert(this.block_chain.length > 1)
      this.caret_node = this.block_chain.shift()
      var char_count = this.caret_node.nodes[this.caret_node.nodes.length-1].char_count()
    }
    else
    {
      this.caret_node = this.block_chain[0].next(this.caret_node)
      if (this.caret_node.is_block())
      {
        this.block_chain.unshift(this.caret_node)
        this.caret_node = this.block_chain[0].start()
      }
      var char_count = this.caret_node.char_count()
    }

    console.assert(undefined !== this.generation_result)
    this.generation_result.move_right(char_count)

    if (!this.block_chain[0].is_node_allowed_front(this.caret_node))
    {
      this.move_right()
    }
  }

  set_caret (caret, direction) {
    this.caret_node = caret
    if (!this.block_chain[0].is_node_allowed_front(caret))
    {
      console.assert(-1 === direction || 1 === direction)
      if (-1 === direction)
      {
        this.move_left()
      }
      if (1 === direction)
      {
        this.move_right()
      }
    }
  }


  select_left (direction = undefined) {
    if (undefined === direction)
    {
      direction = this.has_selection() ? this.selection.direction : 0
    }

    if (!this.has_selection())
    {
      var end_chain = [this.caret_node, ...this.block_chain]
      var start_chain = [this.caret_node, ...this.block_chain]
      direction = -1
      var growing = true
    }
    else if (1 == direction)
    {
      var start_chain = [this.selection.start, ...this.block_chain]
      var end_chain = [this.prev(this.selection.end), ...this.block_chain]
      var growing = false
    }
    else if (-1 == direction)
    {
      var end_chain = [this.selection.end, ...this.block_chain]
      var start_chain = [this.prev(this.selection.start), ...this.block_chain]
      var growing = true
    }

    this.set_selection(start_chain, end_chain, direction, growing)
  }

  select_right (direction = undefined) {
    if (undefined === direction)
    {
      direction = this.has_selection() ? this.selection.direction : 0
    }

    if (!this.has_selection())
    {
      var start_chain = [this.next(this.caret_node), ...this.block_chain]
      var end_chain = [this.next(this.caret_node), ...this.block_chain]
      direction = 1
      var growing = true
    }
    else if (1 == direction)
    {
      var start_chain = [this.selection.start, ...this.block_chain]
      var end_chain = [this.next(this.selection.end), ...this.block_chain]
      var growing = true
    }
    else if (-1 == direction)
    {
      var end_chain = [this.selection.end, ...this.block_chain]
      var start_chain = [this.next(this.selection.start), ...this.block_chain]
      var growing = false
    }

    this.set_selection(start_chain, end_chain, direction, growing)
  }


  set_selection (start_chain, end_chain, direction, is_selection_growing) {
    let start_common_index = start_chain.findIndex((b) => end_chain.includes(b))
    let end_common_index = end_chain.findIndex((b) => start_chain.includes(b))
    if (0 === start_common_index || 0 === end_common_index)
    {
      ++start_common_index
      ++end_common_index
    }
    console.assert(start_common_index > 0)
    console.assert(end_common_index > 0)

    console.assert(start_chain[start_common_index] === end_chain[end_common_index])
    this.selection = {}
    this.selection.direction = direction

    let common_block = start_chain[start_common_index]
    this.selection.start = start_chain[start_common_index-1]
    this.selection.end = end_chain[end_common_index-1]
    console.assert(!common_block.is_end(this.selection.start) || common_block.is_end(this.selection.end))
    console.assert(!common_block.is_start(this.selection.end) || common_block.is_start(this.selection.start))
    if (
      common_block.is_start(this.selection.start)
      || common_block.is_end(this.selection.end)
    )
    {
      if (start_common_index >= start_chain.length - 1)
      {
        console.assert(end_common_index === end_chain.length - 1)
        console.assert(start_common_index === start_chain.length - 1)
        if (common_block.is_start(start_chain[start_common_index-1]))
        {
          start_chain[start_common_index-1] = common_block.start(1)
        }
        if (common_block.is_end(end_chain[end_common_index-1]))
        {
          end_chain[end_common_index-1] = common_block.end(-1)
        }
      }
      else
      {
        ++start_common_index
        ++end_common_index
      }
    }
    common_block = start_chain[start_common_index]
    this.selection.start = start_chain[start_common_index-1]
    this.selection.end = end_chain[end_common_index-1]
    // After setting block_chain, we can use this.prev and this.next.
    this.block_chain = start_chain.slice(start_common_index)
    start_chain = undefined
    end_chain = undefined

    if (common_block.nodes.indexOf(this.selection.start) > common_block.nodes.indexOf(this.selection.end))
    {
      console.log('Inverting selection direction')
      this.caret_node = (direction >= 0) ? this.selection.end : this.prev(this.selection.start)
      console.assert(this.block_chain[0].is_node_allowed_front(this.caret_node))
      this.selection = undefined
    }

    if (undefined !== this.selection)
    {
      if (!this.block_chain[0].is_node_allowed_back(this.selection.start))
      {
        if (is_selection_growing)
        {
          console.log('Growing selection (start) to left')
          this.select_left(-1)
        }
        else
        {
          console.log('Shrinking selection (start) to right')
          this.select_right(-1)
        }
      }
      if (!this.block_chain[0].is_node_allowed_front(this.selection.end))
      {
        if (is_selection_growing)
        {
          console.log('Growing selection (end) to right')
          this.select_right(1)
        }
        else
        {
          console.log('Shrinking selection (end) to left')
          this.select_left(1)
        }
      }

      this.caret_node = (direction >= 0) ? this.selection.end : this.prev(this.selection.start)
    }
  }

  insert_nodes (...nodes) {
    let removed_nodes = this.remove_selection()
    let index = this.block_chain[0].nodes.indexOf(this.caret_node) + 1
    console.assert(index < this.block_chain[0].nodes.length, 'Cannot add after ending block.')
    this.block_chain[0].nodes.splice(index, 0, ...nodes)
    if (nodes.length > 0 && nodes[0].is_block() && removed_nodes.length > 0)
    {
      let chain = nodes[0].paste_selection_on_creation(removed_nodes)
      this.block_chain.splice(0, 0, ...chain)
      this.caret = removed_nodes[removed_nodes.length-1]
      this.selection = {}
      this.selection.start = removed_nodes[0]
      this.selection.end = removed_nodes[removed_nodes.length-1]
      this.selection.direction = 1
    }
  }

  remove_selection () {
    if (undefined === this.selection)
    {
      return []
    }

    const start_index = this.block_chain[0].nodes.indexOf(this.selection.start)
    console.assert(start_index > 0)
    const end_index = this.block_chain[0].nodes.indexOf(this.selection.end, start_index-1)
    console.assert(-1 !== start_index)
    console.assert(-1 !== end_index)
    console.assert(this.block_chain[0].nodes.length-1 !== end_index)
    console.assert(start_index <= end_index)
    let count = end_index - start_index + 1

    let removed_nodes = this.block_chain[0].nodes.splice(start_index, count)
    this.selection = undefined
    this.caret_node = this.block_chain[0].nodes[start_index - 1]
    console.assert(this.block_chain[0].is_node_allowed_front(this.caret_node))
    return removed_nodes
  }

  has_selection () {
    return (undefined !== this.selection)
  }

  is_math_mode () {
    for (let block of this.block_chain)
    {
      if (undefined !== block.is_math_mode)
      {
        return block.is_math_mode
      }
    }
    console.assert(false, 'There must be at least one block (root?) with well-defined is_math_mode.')
  }

  get_control_keyrow () {
    for (let block of this.block_chain)
    {
      if (undefined !== block.control_keyrow)
      {
        return block.control_keyrow
      }
    }
    console.assert(false, 'There must be at least one math block (formula?) with well-defined control_keyrow.')
  }

  push_user_keyrow (keyrow_name) {
    this.get_context_block().user_choosen_keyrows.unshift(keyrow_name)
    this.get_context_block().user_choosen_keyrows.splice(5)
  }

  recomended_keyrows_with_duplicates () {
    let result = [...this.get_context_block().user_choosen_keyrows]
    for (let block of this.block_chain)
    {
      result.splice(result.length, 0, ...block.recomended_keyrows)
    }
    return result
  }

  fix_removed (removed_info, hint = +1) {
    console.assert(hint === 1 || hint === -1)

    // When nothing is removed, removed_info = undefined.
    if (undefined === removed_info)
    {
      return
    }

    let right_or_left = (1 === hint) ? 'right' : 'left'

    this.selection = undefined

    console.log('Removed info:', removed_info)
    let substitute = removed_info[right_or_left]
    let shall_substitute = false
    if (removed_info.removed_nodes.includes(this.caret_node))
    {
      shall_substitute = true
    }

    for (let removed of removed_info.removed_nodes)
    {
      let index = this.block_chain.indexOf(removed)
      if (index !== -1)
      {
        console.log('Removing block that contains caret! Block index in chain:', index)
        console.log('Chain:', this.block_chain)
        shall_substitute = true
        let removed = this.block_chain.splice(0, index + 1)
        console.log('Removed form chain:', removed)
        break
      }
    }

    if (shall_substitute)
    {
      console.log('Curent block nodes:', this.block_chain[0].nodes)
      console.log('Substitute:', substitute)
      console.assert(this.block_chain[0].nodes.includes(substitute))
      this.caret_node = substitute
      this.fix_forbidden(hint)
    }
  }

  fix_forbidden(hint) {
    console.assert(hint === 1 || hint === -1)
    if (!this.block_chain[0].is_node_allowed_front(this.caret_node))
    {
      if (hint > 0)
      {
        this.move_right()
      }
      else
      {
        this.move_left()
      }
    }
  }
}


class TexFormulaStart extends TexLeaf {
  generate_preview (caret, result) {
    if (this.ignore) { return }
    result.is_math_mode = true
    super.generate_preview(caret, result, '', '\\class{formula_math}{')
  }
}

class TexFormulaEnd extends TexLeaf {
  generate_preview (caret, result) {
    if (this.ignore) { return }
    result.is_math_mode = false
    super.generate_preview(caret, result, '}', '')
  }
}

class TexFormula extends TexBlock {
  constructor () {
    let styles = {
      inline: ['${', '}$'],
      block: ['$${', '}$$'],
    }
    let opening = new TexFormulaStart(styles.inline[0])
    let closing = new TexFormulaEnd(styles.inline[1])
    super(opening, closing, styles)
    this.is_math_mode = true
    this.recomended_keyrows = ['numbers', 'operators', 'variables']
    this.control_keyrow = 'formula'
  }
}


class TexOperatorLimit extends TexSubordinatedBlock {
  constructor (left, right) {
    super(left, right)
    this.recomended_keyrows = ['numbers', 'variables', 'operations']
  }
}

class TexOperatorWithLimits extends TexNonEditableBlock {
  constructor (left, right, styles, main_part) {
    super(left, right, styles)

    this.lower = new TexOperatorLimit('_{', '}')
    this.upper = new TexOperatorLimit('^{', '}')
    this.main_part = main_part
    this.append_nodes(this.lower, this.upper, main_part)
  }

  paste_selection_on_creation (nodes) {
    this.main_part.insert_nodes_at_index(1, ...nodes)
    return [this.main_part, this]
  }

  no_limits (caret) {
    this.lower.ignore = true
    this.upper.ignore = true
    caret.fix_forbidden(1)
  }

  only_lower (caret) {
    this.lower.ignore = false
    this.upper.ignore = true
    caret.fix_forbidden(1)
  }

  lower_and_upper (caret = dumb_caret) {
    this.lower.ignore = false
    this.upper.ignore = false
  }
}


/*
* Integral.
*/

class TexIntegralFunction extends TexSubordinatedBlock {
  constructor () {
    super('{', '}')
    this.recomended_keyrows = ['functions', 'variables', 'operations', 'numbers']
  }
}

class TexIntegralDifferential extends TexSubordinatedBlock {
  constructor () {
    super('{\\,\\mathrm{d}', '}')
    this.recomended_keyrows = ['variables', 'vectors']
  }
}

class TexIntegral extends TexOperatorWithLimits {
  constructor () {
    let styles = {
      int: ['{{\\int}', '}'],
      iint: ['{{\\iint}', '}'],
      iiint: ['{{\\iiint}', '}'],
      oint: ['{{\\oint}', '}'],
      oiint: ['{{\\oiint}', '}'],
      oiiint: ['{{\\oiiint}', '}'],
    }
    super(...styles.int, styles, new TexIntegralFunction())
    this.recomended_keyrows = ['functions', 'numbers', 'variables']
    this.control_keyrow = 'integral'

    this.differentials = []
    this.differential_letters = ['x', 'y', 'z', 'w', 'k']
    this.add_differential()
  }

  add_differential (caret = undefined) {
    let new_differential = new TexIntegralDifferential()
    this.differentials.push(new_differential)

    if (this.differential_letters.length > this.differentials.length)
    {
      let letter = this.differential_letters[this.differentials.length-1]
      new_differential.append_nodes(new TexLeaf(letter))
    }
    this.append_nodes(new_differential)
  }

  remove_differential (caret) {
    if (0 === this.differentials.length)
    {
      console.log('No differential to remove.')
      return
    }

    this.differentials.pop()
    caret.fix_removed(this.remove_node(-2), -1)
  }
}


class TexMatrixEntry extends TexSubordinatedBlock {
  constructor () {
    super('{', '}')
  }
}

class TexMatrixLine extends TexNonEditableBlock {
  constructor (n_entries) {
    super('', '')
    // NonEditable and Subordinated. Multiple inheritance. :-/
    this._is_subordinated_block = true
    this.insert_nodes_at_index(1, new TexMatrixEntry())
    this.n_entries = 1
    while (this.n_entries < n_entries)
    {
      this.add_entry()
    }
  }

  add_entry (caret = undefined) {
    this.insert_nodes_at_index(-1, new TexLeaf('&'), new TexMatrixEntry())
    this.n_entries += 1
  }

  del_entry (caret) {
    if (this.n_entries <= 1)
    {
      console.log('Will not delete the last column')
      return
    }

    this.n_entries -= 1

    caret.fix_removed(this.remove_node(-2)) // TexMatrixEntry()
    caret.fix_removed(this.remove_node(-2)) // &
  }
}

class TexMatrix extends TexNonEditableBlock {
  constructor (n_lines = 2, n_columns = 2) {
    let styles = {
      none: ['\\begin{matrix}', '\\end{matrix}'],
      pmatrix: ['\\begin{pmatrix}', '\\end{pmatrix}'],
      bmatrix: ['\\begin{bmatrix}', '\\end{bmatrix}'],
      Bmatrix: ['\\begin{Bmatrix}', '\\end{Bmatrix}'],
      vmatrix: ['\\begin{vmatrix}', '\\end{vmatrix}'],
      Vmatrix: ['\\begin{Vmatrix}', '\\end{Vmatrix}'],
    }
    super(...styles.pmatrix, styles)
    this.recomended_keyrows = ['numbers', 'variables', 'operations']
    this.control_keyrow = 'matrix'

    this.n_columns = n_columns
    this.insert_nodes_at_index(1, new TexMatrixLine(this.n_columns))
    this.n_lines = 1
    while (this.n_lines < n_lines)
    {
      this.add_line()
    }
  }

  paste_selection_on_creation (nodes) {
    this.nodes[1].nodes[1].insert_nodes_at_index(1, ...nodes)
    return [this.nodes[1].nodes[1], this.nodes[1], this]
  }

  add_line (caret = undefined) {
    this.insert_nodes_at_index(-1, new TexLeaf('\\\\'), new TexMatrixLine(this.n_columns))
    this.n_lines += 1
  }

  del_line (caret) {
    if (this.n_lines <= 1)
    {
      console.log('Will not delete the last line')
      return
    }

    this.n_lines -= 1

    caret.fix_removed(this.remove_node(-2)) // TexMatrixLine()
    caret.fix_removed(this.remove_node(-2)) // \\
  }

  add_column (caret = undefined) {
    this.n_columns += 1
    for (let line of this.nodes)
    {
      if (line instanceof TexMatrixLine)
      {
        line.add_entry(caret)
      }
    }
  }

  del_column (caret) {
    if (this.n_columns <= 1)
    {
      console.log('Will not remove last column')
      return affects_caret
    }

    this.n_columns -= 1
    for (let line of this.nodes)
    {
      if (line instanceof TexMatrixLine)
      {
        line.del_entry(caret)
      }
    }
  }
}

class TexBigOperator extends TexOperatorWithLimits {
  constructor () {
    let styles = {
      sum: ['{{\\sum}', '}'],
      prod: ['{{\\prod}', '}'],
      cup: ['{{\\bigcup}', '}'],
      cap: ['{{\\bigcap}', '}'],
      oplus: ['{{\\bigoplus}', '}'],
      otimes: ['{{\\bigotimes}', '}'],
      wedge: ['{{\\bigwedge}', '}'],
      vee: ['{{\\bigvee}', '}'],
    }
    super(...styles.sum, styles, new TexBlock('', ''))

    this.recomended_keyrows = ['numbers', 'variables', 'operations']
    this.control_keyrow = 'big_operator'
  }
}

class TexSet extends TexNonEditableBlock {
  constructor () {
    super('\\left\\{', '\\right\\}')
    this.description = new TexSubordinatedBlock('', '')
    this.condition = new TexSubordinatedBlock(':{\\,', '}')
    this.insert_nodes_at_index(-1, this.description, this.condition)

    this.recomended_keyrows = ['numbers', 'variables', 'functions']
    this.control_keyrow = 'sets'
  }
  set_condition (caret) {
    this.condition.ignore = false
  }

  remove_condition (caret) {
    if (this.condition.ignore)
    {
      console.log('No condition to remove.')
      return
    }
    this.condition.ignore = true
    caret.fix_forbidden(-1)
  }
}

// TODO: use a special two-content block
class TexFrac extends TexNonEditableBlock {
  constructor () {
    let styles = {
      no_parenthesis: ['\\frac', ''],
      parenthesis: ['\\left(\\frac', '\\right)'],
    }
    super(...styles.no_parenthesis, styles)
    this.numerator = new TexSubordinatedBlock('{', '}')
    this.denominator = new TexSubordinatedBlock('{', '}')
    this.insert_nodes_at_index(-1, this.numerator, this.denominator)

    this.recomended_keyrows = ['numbers', 'operations', 'variables']
    this.control_keyrow = 'frac'
  }

  paste_selection_on_creation (nodes) {
    this.numerator.insert_nodes_at_index(1, ...nodes)
    return [this.numerator, this]
  }

  block_parenthesis () {
    this.set_style('parenthesis')
  }

  block_no_parenthesis () {
    this.set_style('no_parenthesis')
  }
}

// TODO: use a special two-content block
class TexPow extends TexNonEditableBlock {
  constructor () {
    super('', '')
    let base_styles = {
      no_parenthesis: ['{', '}'],
      parenthesis: ['\\left(', '\\right)'],
    }
    this.base = new TexSubordinatedBlock('{', '}', base_styles)
    this.exponent = new TexSubordinatedBlock('^{', '}')
    this.insert_nodes_at_index(-1, this.base, this.exponent)

    this.recomended_keyrows = ['numbers', 'operations', 'variables']
    this.control_keyrow = 'pow'
  }

  paste_selection_on_creation (nodes) {
    this.base.insert_nodes_at_index(1, ...nodes)
    return [this.base, this]
  }

  main_part_parenthesis () {
    this.base.set_style('parenthesis')
  }

  main_part_no_parenthesis () {
    this.base.set_style('no_parenthesis')
  }
}
// TODO: use a special two-content block
class TexSqrt extends TexNonEditableBlock {
  constructor () {
    super('\\sqrt', '')
    this.control_keyrow = 'sqrt'
    this.main_part = new TexSubordinatedBlock('{', '}')

    this.exponent = new TexSubordinatedBlock('[', ']')
    this.exponent.ignore = true
    this.exponent_fixed = new TexLeaf('[2]')
    this.exponent_fixed.ignore = true

    this.insert_nodes_at_index(-1, this.exponent_fixed, this.exponent, this.main_part)

    this.recomended_keyrows = ['numbers', 'variables']
    this.control_keyrow = 'sqrt'
  }

  paste_selection_on_creation (nodes) {
    this.main_part.insert_nodes_at_index(1, ...nodes)
    return [this.main_part, this]
  }

  set_exponent (caret) {
    this.exponent.ignore = false
    this.exponent_fixed.ignore = true
  }

  set_exponent_two (caret) {
    this.remove_exponent(caret)
    this.exponent_fixed.contents = '[2]'
    this.exponent_fixed.ignore = false
  }

  set_exponent_three (caret) {
    this.remove_exponent(caret)
    this.exponent_fixed.contents = '[3]'
    this.exponent_fixed.ignore = false
  }

  remove_exponent (caret) {
    this.exponent_fixed.ignore = true
    if (this.exponent.ignore)
    {
      console.log('No exponent to remove.')
      return
    }
    this.exponent.ignore = true
    caret.fix_forbidden(1)
  }
}
