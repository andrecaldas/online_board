"use strict";

Vue.component('message_board', {
  props: {
    messages: {
      type: Array,
      required: true,
    },
  },
  data: function () { return {
    must_scroll: true,
    next_rendered_index: 0,
  }},

  beforeUpdate: function () {
    this.check_scroll()
  },

  updated: function () {
    this.render_mathjax()
  },

  methods: {
    render_mathjax: function (e) {
      let message_slice = this.$refs.message_elements.slice(this.next_rendered_index);
      this.next_rendered_index += message_slice.length;
      if (message_slice.length !== 0)
      {
        this.$nextTick(() => {
          let p = MathJax.typesetPromise(message_slice);
          p.then((r, x) => {this.scroll(message_slice.pop())});
        })
      }
    },

    // Lots of javascript could be eliminated if Firefox cared
    //for this 6 year old bug!!!
    // We should use column-reversed, but we use javascript. :-(
    // https://bugzilla.mozilla.org/show_bug.cgi?id=1042151
    check_scroll: function () {
      this.must_scroll = (this.$el.scrollHeight <= 200 + this.$el.clientHeight + this.$el.scrollTop)
    },

    scroll: function (e) {
      if (this.must_scroll)
      {
        e.scrollIntoView()
      }
      else
      {
      // TODO.
//        feedback_queue.push('Nova mensagem abaixo...')
      }
    },
  },
  template: `
    <div id="the_board">
      <p v-for="m in messages" :id="'msg-' + m.id"
        ref="message_elements"
        :class="{owner: m.is_owner, not_owner: !m.is_owner}">
        <span class="user_info">{{ m.user_print_name }} ({{ m.user_name }})</span>
        {{ m.text }}
      </p>
    </div>
  `
})
