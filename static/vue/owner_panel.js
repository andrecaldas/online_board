"use strict";

Vue.component('owner_panel', {
  template: `
    <div id="input_box" class="owner_panel">
      <form id="board_form" action="append_text" method="post"
          onsubmit="post_line(); preview_input(''); return false;">
        <tex_input
          placeholder="Digite texto com código latex!">
        </tex_input>
      </form>
      <div id="input_preview" tabindex="-1"></div>
    </div>
  `
})
