"use strict";

Vue.component('user_info', {
  props: {
    user_name: {type: String, required: true},
    user_info: {type: Object, required: true},
  },

  data () { return {
    print_name: ''
  }},

  // TODO: Move all comunication with server to app.
  // Then, use prop "user_info" to get user_name.
  watch: {
    user_name () {
      this.update_user_info ()
    }
  },

  methods: {
    update_user_info ()
    {
      $.post({
        url : '/user_info',
        dataType: 'json',
        success : (data) => {
          this.print_name = data.print_name
        },
      })
    },
  },

  template: `
    <div id="user_info">
      <ul>
        <li v-if="user_info.is_guest" id="print_name">Visitante</li>
        <li v-else id="print_name">{{ print_name }}</li>
      </ul>
    </div>
  `
})
