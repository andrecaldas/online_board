"use strict";

Vue.component('registration_form', {
  props: [],
  data: function () { return {
    print_name: '',
    user_name: '',
    email: '',
    password: '',
  }},
  template: `
    <form class="register_form" @submit.prevent="ajax_registration" method="post" action="/register">
      <input type="text" v-model="user_name" placeholder="Login" />
      <input type="text" v-model="print_name" placeholder="Nome do usuário" />
      <input type="email" v-model="email" placeholder="Seu e-mail" />
      <input type="password" v-model="password" placeholder="Senha" />
      <input type="submit" value="Criar Novo Usuário" />
    </form>
  `
})
