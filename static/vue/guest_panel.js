"use strict";

Vue.component('guest_panel', {
  template: `
  <div class="guest_panel">
    <login_form
      v-on="$listeners"
    ></login_form>
    <p>
      Ou então...
      <a href="/">faça seu cadastro</a>!
    </p>
  </div>
  `
})
