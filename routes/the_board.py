import json

from flask import request, render_template, redirect, url_for, make_response

from board_app import app
from board_app import logged_user

import board_model as model
from board_db import db


@app.route("/board/<owner_name>/<course_name>/<board_name>")
@app.route("/board/<owner_name>/<course_name>/<board_name>/")
def board(**args):
    return redirect(url_for('board_show', **args))

@app.route("/board/<owner_name>/<course_name>/<board_name>/show")
def board_show(owner_name, course_name, board_name):
    user = logged_user()
    board = model.get_board(owner_name, course_name, board_name)
    return render_template('board.html', user=user, board=board)


@app.route("/board/<owner_name>/<course_name>/<board_name>/get_messages", defaults={'last': -1})
@app.route("/board/<owner_name>/<course_name>/<board_name>/get_messages/<last>")
def get_messages(owner_name, course_name, board_name, last):
    board = model.get_board(owner_name, course_name, board_name)
    last = int(last)
    activities = model.get_board_logs(board, -5)
    alog = [
        {'timestamp': a.timestamp.timestamp(), 'log_text': a.log_text}\
        for a in activities
    ]
    authorized_peers = [bp.peer.name for bp in model.get_authorized_peers(board)]

    messages_info = [
        {
            'id': m.scrambled_id(),
            'text': m.text,
            'is_owner': m.user == board.course.owner,
            'user_name': m.user.name,
            'user_print_name': m.user.print_name,
        }
      for m in board.get_messages_after_scrambled_id(last)
    ]

    info = {
        'activity_log': alog,
        'authorized_peers': authorized_peers,
        'messages': messages_info,
    }
    user = logged_user()
    response = make_response(json.dumps(info))
    if not user.is_guest():
        # TODO: Sendo cookie only when user_name changes. Is it worth?
        response.set_cookie('user_name', str(user.name), samesite='strict')
    return response


@app.route("/board/<owner_name>/<course_name>/<board_name>/append_text", methods=["POST"])
def append_text(owner_name, course_name, board_name):
    user = logged_user()
    message = request.form['board_input']
    board = model.get_board(owner_name, course_name, board_name)
    if board.course.owner != user:
      if not model.is_peer_authorized(board, user):
          raise Exception('No permission to write here!')
    board.append_message(db.session, user, message)
    db.session.commit()
    url = url_for(
      'board',
      owner_name=owner_name,
      course_name=course_name,
      board_name=board_name
    )
    return redirect(url)


@app.route("/board/<owner_name>/<course_name>/<board_name>/check_peer", methods=['POST', 'GET'])
def check_peer(owner_name, course_name, board_name):
    user = logged_user()
    board = model.get_board(owner_name, course_name, board_name)
    model.check_board_peer(db.session, board, user)
    db.session.commit()
    return 'OK!'


@app.route("/board/<owner_name>/<course_name>/<board_name>/peer_list", methods=['POST', 'GET'])
def peer_list(owner_name, course_name, board_name):
    user = logged_user()
    board = model.get_board(owner_name, course_name, board_name)
    if user.is_guest():
        raise Exception('Sorry! Only the course owner can view this list: {} != {}!'.format(board.course.owner, user))
    peers = model.get_board_peers(db.session, board, user)
    return json.dumps(peers)


@app.route("/board/<owner_name>/<course_name>/<board_name>/wave_hand", methods=['POST', 'GET'])
def wave_hand(owner_name, course_name, board_name):
    user = logged_user()
    if(user.is_guest()):
        raise Exception('Guest cannot wave hand.')
    board = model.get_board(owner_name, course_name, board_name)
    now = model.wave_hand(db.session, board, user)
    return json.dumps(now.timestamp())


@app.route("/board/<owner_name>/<course_name>/<board_name>/lower_hand", methods=['POST', 'GET'])
def lower_hand(owner_name, course_name, board_name):
    user = logged_user()
    if(user.is_guest()):
        raise Exception('Guest cannot wave hand.')
    board = model.get_board(owner_name, course_name, board_name)
    model.wave_hand(db.session, board, user, False)
    return 'OK!'


@app.route("/board/<owner_name>/<course_name>/<board_name>/authorize_peer", methods=['POST'])
def authorize_peer(owner_name, course_name, board_name):
    user = logged_user()
    board = model.get_board(owner_name, course_name, board_name)
    if board.course.owner != user:
      raise Exception('No permission to authorize here!')
    peer_name = request.form.get('peer_name')
    print('Peer name:', peer_name)
    model.authorize_peer(db.session, board, peer_name)
    return 'OK!'


@app.route("/board/<owner_name>/<course_name>/<board_name>/revoke_peer", methods=['POST'])
def revoke_peer(owner_name, course_name, board_name):
    user = logged_user()
    board = model.get_board(owner_name, course_name, board_name)
    if board.course.owner != user:
      raise Exception('No permission to revoke here!')
    peer_name = request.form.get('peer_name')
    model.authorize_peer(db.session, board, peer_name, do_authorize=False)
    return 'Revoked!'
