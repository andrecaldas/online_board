import json

from flask import request, render_template, redirect, url_for, make_response

from board_app import app
from board_app import logged_user
from board_app import login as do_login
from board_app import logout as do_logout

import board_model as model
from board_db import db


@app.route("/login", methods=["POST"])
def login():
    user_name = request.form.get('user_name')
    password = request.form.get('password')
    user = do_login(user_name, password)
    response = make_response(redirect(url_for('dash_board')))
    response.delete_cookie('user_id')
    if not user.is_guest():
        # TODO: Sendo cookie only when user_id changes. Is it worth?
        response.set_cookie('user_name', str(user.name), samesite='strict')
    return response


@app.route("/logout", methods=["POST", "GET"])
def logout():
    do_logout()
    response = make_response(redirect(url_for('index')))
    response.delete_cookie('user_name')
    response.set_cookie('user_name', '', samesite='strict')
    return response


@app.route("/register", methods=["POST"])
def register():
    name = request.form.get('user_name')
    password = request.form.get('password')
    email = request.form.get('email')
    print_name = request.form.get('print_name')
    model.create_user(
            db.session,
            name=name,
            email=email,
            password=password,
            print_name=print_name,
    )
    db.session.commit()
    do_login(name, password)
    return redirect(url_for('dash_board'))


@app.route("/user_info", methods=["POST"])
def user_info():
    user = logged_user()
    info = {
        'name': user.name,
        'print_name': user.print_name,
    }
    return json.dumps(info)


@app.route("/dash_board")
def dash_board():
    user = logged_user()
    return render_template('dash_board.html', user=user)
