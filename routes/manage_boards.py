import json

from flask import request, render_template, redirect, url_for
from flask import send_from_directory

from board_app import app
from board_app import logged_user

import board_model as model
from board_db import db

@app.route("/course/<owner_name>/<course_name>")
@app.route("/course/<owner_name>/<course_name>/")
def course(**args):
    return redirect(url_for('course_show', **args))

@app.route("/course/<owner_name>/<course_name>/show")
def course_show(owner_name, course_name):
    user = logged_user()
    course = model.get_course(owner_name, course_name)
    return render_template('course.html', user=user, course=course)


@app.route("/create_course", methods=["POST"])
def create_course():
    args = {}
    args['owner'] = logged_user()
    args['name'] = request.form.get('name')
    args['print_name'] = request.form.get('print_name')
    args['description'] = request.form.get('description')
    model.create_course(db.session, **args)
    db.session.commit()
    return redirect(url_for('dash_board'))


@app.route("/create_board/<course_name>", methods=["POST"])
def create_board(course_name):
    args = {}
    args['owner'] = logged_user()
    args['course_name'] = course_name
    args['name'] = request.form.get('name')
    args['print_name'] = request.form.get('print_name')
    args['description'] = request.form.get('description')
    model.create_board(db.session, **args)
    db.session.commit()
    return redirect(url_for('dash_board'))
